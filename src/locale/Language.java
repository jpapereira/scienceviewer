/**
 * 
 */
package locale;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * @author bluetc-1205
 *
 */
public class Language {
	private Locale currentLocale;
	private ResourceBundle messages;
	
	private static Language instance;
	
	public static Language lang(){
		if( instance == null )
			instance = new Language();
		return instance;
	}
	
	public String getTranslation( String value ){
		return messages.getString(value);
	}
	
	private Language(){
		String language = System.getProperty("user.language");;
		String country = new String("US");
		currentLocale = new Locale(language);
		
		messages = ResourceBundle.getBundle("locale.MessagesBundle", currentLocale);
	}
}
