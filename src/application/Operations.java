package application;
import java.io.File;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import viewer.main.ProgressBar;

import core.*;

/**
 * @author bluetc-1205
 *
 */
public class Operations {
	
	/**
	 * @param startDNA
	 * @param toCheckDNA
	 * @return
	 * @throws ApplicationException
	 */
	public static boolean checkDifference( String startDNA, String toCheckDNA) throws ApplicationException{
		
		Dna start;
		try {
			start = new Dna(startDNA);
		} catch (ApplicationException e) {
			// TODO Auto-generated catch block
			throw new ApplicationException(e.getMessage() + " On Start DNA sequence!!");
			
		}
		Dna toCheck;
		try {
			toCheck = new Dna(toCheckDNA);
		} catch (ApplicationException e) {
			// TODO Auto-generated catch block
			throw new ApplicationException(e.getMessage() + " On DNA sequence to check!!");
		}
		
		
		
		return start.compare(toCheck);
		
	}
	/**
	 * @param sequence
	 * @return
	 * @throws ApplicationException
	 */
	public static String getAminoacidFASTa( String sequence ) throws ApplicationException{
		Dna dna;
		try {
			dna = new Dna(convertFromFASTa(sequence));
		} catch (ApplicationException e) {
			// TODO Auto-generated catch block
			throw new ApplicationException(e.getMessage() + " On Start DNA sequence!!");
			
		}
		return dna.getAminoacidString();
	}
	
	/**
	 * @param sequence
	 * @param throwExceptions
	 * @return
	 * @throws ApplicationException
	 */
	public static boolean checkSequenceFASTa( String sequence, boolean throwExceptions ) throws ApplicationException{
		return Dna.checkDNASequence(convertFromFASTa(sequence), throwExceptions);
		
	}
	
	public static String convertFromFASTa( String sequence ){
		return sequence.replaceAll("\\s", "").replaceAll("\\n", "").replaceAll("\\r", "");
	}
	
	public static char[][] retrieveTwoDNAFromFASTa( String sequence1, String sequence2 ){
		
		char[][] result = new char[2][];
		String seq1 = convertFromFASTa(sequence1);
		String seq2 = convertFromFASTa(sequence2);
		result[0] = seq1.toCharArray();
		result[1] = seq2.toCharArray();
		
		return result;
	}
	
	public static List<String> retrieveComparedDNASeqFASTa( String baseSequence, String sequence2 ){
		char[][] result = retrieveTwoDNAFromFASTa( baseSequence , sequence2 );
		String[] outResult = new String[2];
		int maxLength = (result[0].length>result[1].length?result[0].length:result[1].length);
		SmithWaterman a = new SmithWaterman(new String(result[0]),new String(result[1]));
		System.out.println("Going to print alignment");
		a.printAlignments();
		System.out.println("Going to retrieve alignment");
		return a.retrieveAlignments();
	}
	public static List<String> compareTwoStrings( String base, String check){
		List<String> res =  new ArrayList<String>(); 
		for( int i = 0 ; i < base.length() ; i++){
			if( base.charAt(i) != check.charAt(i))
				res.add("The position ["+i+"] is incorrect");
		}
		return res;
	}
	
	public static String retrieveAntiparallel( String sequence ) throws ApplicationException{
		
		return Dna.antiparallel(sequence);
	}
	public static String retrieveComplement( String sequence ) throws ApplicationException{
		
		return Dna.complement(sequence);
	}
	
	
	public static String retrieveInvert( String sequence ) throws ApplicationException{
		return Dna.invert(sequence);
	}
	
	public static ArrayList<ArrayList<String>> compareExcel( CompareExcel.CompareType type,
															 int reference, 
															 File [] comparables,
															 int numComparables,
															 ProgressBar bar) throws ApplicationException{
		
		CompareExcel cmp = new CompareExcel( comparables,reference,bar,(int)(1./numComparables*100));
		System.out.println("Values sent: "+(1./comparables.length*100));
		return cmp.compare(type, 0, -1, 0);
	}
	public static ArrayList<ArrayList<ArrayList<String>>> compareExcelAll( CompareExcel.CompareType type, 
																		   int reference, 
																		   File [] comparables,
																		   int numComparables,
																		   ProgressBar bar) throws ApplicationException{
		ArrayList<ArrayList<ArrayList<String>>> result = new ArrayList<ArrayList<ArrayList<String>>>();
		int aux = 0;
		int i = 0;
		int max = 0;
		for( int l = 0; l < numComparables ; l++)
			max += l;
		for( File file : comparables ){
			if( numComparables < (i+1) )
				break;
			int j = 0;
			ArrayList<ArrayList<String>> auxResult = new ArrayList<ArrayList<String>>();
			for( File f : comparables ){
				if( numComparables < (j+1) )
					break;
				System.out.println("i["+i+"]j["+j+"]");
				/*if( i > j ){
					auxResult.add(result.get(j).get(i));
					j++;
					continue;
				}*/
				
				if( file == f ){
					auxResult.add(new ArrayList<String>());
					j++;
					continue;
				}
				File [] compFiles = new File[2];
				compFiles[0] =file;
				compFiles[1] = f;
				CompareExcel cmp = new CompareExcel( compFiles,0, bar, (int)(100./max));
				auxResult.add(cmp.compare(type, 0, -1, 0).get(1));
				j++;
			}
			result.add(auxResult);
			i++;
		}
		return result; 
	}
	public static Hashtable<String,ArrayList<Character>> retrieveMatrix( File [] comparables,
																int numComparables,
			 													ProgressBar bar ) throws ApplicationException{
		CompareExcel cmp = new CompareExcel( comparables,0,bar,(int)(1./numComparables*100));
		System.out.println("Values sent: "+(1./comparables.length*100));
		return cmp.retrieveMatrix();
	}
}
