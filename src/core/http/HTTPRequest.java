package core.http;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.Proxy;
import java.net.URL;

public class HTTPRequest {
	 private URL url;
	 private HttpURLConnection connection;
	 private Proxy proxy;
	 
	 public HTTPRequest(Proxy proxy ){
		 this.setProxy(proxy);
		 connection = null;
		 url = null;
	 }
	 public HTTPRequest(URL url, Proxy proxy ){
		 this.setUrl(url);
		 this.setProxy(proxy);
		 connection = null;
	 }
	 public boolean openConnection(String targetURL){
		 //Create connection
		 try{
	     url = new URL(targetURL);
	     if( proxy == null )
	    	 connection = (HttpURLConnection)url.openConnection();
	     else
	    	 connection = (HttpURLConnection)url.openConnection(proxy);
		 } catch (Exception e) {
		      e.printStackTrace();
		      return false;
		 }
		 return true;
	 }
	 public void closeConnection(){
		 if( connection != null ){
			 connection.disconnect();
		 }
		 
	 }
	 public String executeGet( String targetUrl, String urlParameters) throws IOException{
		 if( connection != null)
			 closeConnection();
		 if( !openConnection( targetUrl))
			 return null;
		 String result = executeGet( urlParameters );
		 closeConnection();
		 return result;
		 
	 }
	 public String executeGet( String urlParameters) throws IOException
	  {
	    
		if( connection == null )
			return null;
	    try {
	      
	      connection.setRequestMethod("GET");
	      connection.setRequestProperty("Content-Type", 
	           "application/x-www-form-urlencoded");
				
	      connection.setRequestProperty("Content-Length", "" + 
	               Integer.toString(urlParameters.getBytes().length));
	      connection.setRequestProperty("Content-Language", "en-US");  
				
	      connection.setUseCaches (false);
	      connection.setDoInput(true);
	      connection.setDoOutput(true);

	      //Send request
	      DataOutputStream wr = new DataOutputStream (
	                  connection.getOutputStream ());
	      wr.writeBytes (urlParameters);
	      wr.flush ();
	      wr.close ();

	      //Get Response	
	      InputStream is = connection.getInputStream();
	      BufferedReader rd = new BufferedReader(new InputStreamReader(is));
	      String line;
	      StringBuffer response = new StringBuffer(); 
	      while((line = rd.readLine()) != null) {
	        response.append(line);
	        response.append('\r');
	      }
	      rd.close();
	      return response.toString();
	    }catch( java.io.IOException e ){
	    	throw e;
	    } catch (Exception e) {

	      e.printStackTrace();
	      return null;
	    } 
	  }
	/**
	 * @return the url
	 */
	public URL getUrl() {
		return url;
	}
	/**
	 * @param url the url to set
	 */
	public void setUrl(URL url) {
		this.url = url;
	}
	/**
	 * @return the proxy
	 */
	public Proxy getProxy() {
		return proxy;
	}
	/**
	 * @param proxy the proxy to set
	 */
	public void setProxy(Proxy proxy) {
		this.proxy = proxy;
	}
}
