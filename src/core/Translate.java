/**
 * 
 */
package core;

import java.util.Hashtable;



/**
 * @author bluetc-1205
 *
 */
public class Translate {
	enum Languages{
		ENGLISH,SPANISH;
	}
	private static Translate singletonObj;
	AllLanguages languages;
	public Translate(Languages lang){
		
		switch(lang){
			case SPANISH:
			
			case ENGLISH:
			default:
				languages = new TransEnglish();
				break;
		}
	}
	public String text( String label ){
		return languages.text(label);
	}
}



class AllLanguages{
	Hashtable<String,String> langHash;
	public String text( String label ){
		if( langHash.containsKey(label))
			return langHash.get(label);
		else
			return new String("Not Defined");
	}
}

class TransEnglish extends AllLanguages{
	 
	public TransEnglish(){
		langHash.put("FILE", "File");
		langHash.put("EXIT", "Exit");
		langHash.put("EDIT", "Edit");
		langHash.put("COPY", "Copy");
		langHash.put("PASTE", "Paste");
		langHash.put("CUT", "Cut");
		langHash.put("SEQUENCE", "Sequence");
		langHash.put("COMPARATOR", "Comparator");
		langHash.put("OPERATIONS", "Operations");
		
	}
}