package core;

import java.io.IOException;
import java.net.Proxy;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map.Entry;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import core.http.HTTPRequest;

public class DictyDatabase {
	public enum SequenceType{
		PROTEIN;
	};
	private final String url ="http://dictybase.org/db/cgi-bin/dictyBase/yui/get_fasta.pl";
	private final int poolSize = 10;
	private HTTPRequest req;
		
	
	ArrayList<Connection> connectionPool = new ArrayList<Connection>();
	
	BlockingQueue<Entry<String,String>> sharedQueue = new LinkedBlockingQueue<Entry<String,String>>();
	
	BlockingQueue<Entry<String,String>> outputQueue = new LinkedBlockingQueue<Entry<String,String>>();
	
	Hashtable<String,String> results = new Hashtable<String,String>();
	
	Hashtable<String,Integer> blackList = new Hashtable<String,Integer>();
	
	private boolean stopWorking = false;
	
	class miniWorker extends ViewerThread{
		BlockingQueue<Entry<String,String>> myQueue;
		Hashtable<String,Integer> blackList = new Hashtable<String,Integer>();
		
		Hashtable<String,String> resultHandler;
		public miniWorker(BlockingQueue<Entry<String,String>> outputQueue,Hashtable<String,String> results){
			myQueue = outputQueue;
			resultHandler = results;
		}
		@Override
		protected void work() {
			try{
				Entry<String,String> entry = myQueue.take();
				synchronized( resultHandler ){
					System.out.println("Found: "+entry.getKey());
					if( null == entry.getValue() )
						resultHandler.put(entry.getKey(), null);
					else
						resultHandler.put(entry.getKey(), entry.getValue());
					resultHandler.notifyAll();
					
				}
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	miniWorker w;
	
	public DictyDatabase( Proxy proxy ) throws Exception{
		req = new HTTPRequest(proxy);
		for( int i = 0 ;i < poolSize ; i++){
			connectionPool.add( new Connection(sharedQueue,outputQueue,blackList,proxy,url));
			connectionPool.get(i).start();
		}
		w = new miniWorker( outputQueue, results);
		w.start();
	}
	
	private String sequenceType( SequenceType type ){
		switch( type ){
		case PROTEIN:
			return "Protein";
		default:
			return null;
		}
	}
	private String buildParameters( SequenceType type, String primaryId){
		return "decor=1&primary_id="+primaryId+"&sequence="+sequenceType(type);
	}
	
	public void search( SequenceType type, String toSearch){
		synchronized( results ){
			if( results.containsKey(toSearch))
				return;
		}
		
		
		try {
			//System.out.println("Search demanded enqueuing request!");
			sharedQueue.put(new AbstractMap.SimpleEntry<String,String>(toSearch,buildParameters(type, toSearch)));
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	public String retrieveSearch( String toSearch ){
		/*while( true ){*/
		System.out.println("Retrieving: "+toSearch);
			synchronized( results ){
				while( !results.containsKey(toSearch) && !stopWorking ){
					try {
						results.wait();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				if( results.containsKey(toSearch) )
					return results.get(toSearch);
				else
					return null;
			}
			/*try {
				System.out.println("waiting on search!");
				Entry<String,String> res = outputQueue.take();
				System.out.println("Retrieved value from search!");
				results.put(res.getKey(),res.getValue());
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				try {
					Thread.sleep(100);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			
		}
		
		return results.get(toSearch);*/
	}
	
	public String parse( String str ){
		return HTTPParse.parse(str);
	}
	public void stopWorking(){
		w.stopThread();
		stopWorking = false;
		for( Connection con:connectionPool )
			con.stopThread();
		synchronized( results ){
			results.notifyAll();
		}
	}
}

class Connection extends ViewerThread {
	
	BlockingQueue<Entry<String,String>> sharedQueue;
	BlockingQueue<Entry<String,String>> outputQueue;
	private HTTPRequest req;
	private String targetUrl;
	
	private volatile Hashtable<String,Integer> blackList;
	
	
	public Connection( BlockingQueue<Entry<String,String>> queue,
					   BlockingQueue<Entry<String,String>> outputQueue,
					   Hashtable<String,Integer> blackList, 
					   Proxy proxy, 
					   String targetUrl){
		sharedQueue = queue;
		this.outputQueue = outputQueue;
		req = new HTTPRequest(proxy);
		this.targetUrl = targetUrl;
		this.blackList = blackList;
	}
	


	@Override
	protected void work() {
		Entry<String,String> parameters = null;
		try {
			//System.out.println("Connector waiting on sharedQueue");
			parameters = sharedQueue.take();
			//System.out.println("Have work starting http request");
			String httpPage = req.executeGet(targetUrl, parameters.getValue());
			String result = null;
			if( null != httpPage){
				result = HTTPParse.parse(httpPage);
				char[] splitter = new char[1];
				splitter[0] = (char)13;
				result = result.replaceAll(new String(splitter), "");
			}
			//System.out.println("Enqueueing request");
			outputQueue.put( new AbstractMap.SimpleEntry<String,String>(parameters.getKey(),result));
			Thread.sleep(20);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			int amount = 0;
			System.out.println("Need to Blacklisting:"+parameters.getKey());
			synchronized( blackList){
				System.out.println("Blacklisting:"+parameters.getKey());
				if( blackList.containsKey(parameters.getKey())){
					
					amount = blackList.get(parameters.getKey())+1;
					System.out.println("Contains:"+amount);
				}
				System.out.println("Amount:"+amount);
				if( amount > 10 ){
					try {
						outputQueue.put( new AbstractMap.SimpleEntry<String,String>(parameters.getKey(),""));
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}else{
					System.out.println("Saving:"+amount);
					blackList.put(parameters.getKey(), amount);
					try {
						sharedQueue.put(parameters);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			}
			
			
		}
	}
	
}

class HTTPParse{
	public static String parse( String html ){
		
		String[] result = html.split("</?pre>");
		/*for( String re: result)
			System.out.println(re.toString());*/
		if( result.length == 3 )
			return result[1];
		return null;
		
	}
}
