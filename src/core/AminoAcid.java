package core;

import java.util.Hashtable;

class AminoacidNotFoundException extends ApplicationException{
	public AminoacidNotFoundException(){}
}
class AminoacidStopSequenceException extends ApplicationException{
	public AminoacidStopSequenceException(){}
}
class AminoacidInvalidSequenceException extends ApplicationException{
	public AminoacidInvalidSequenceException(){}
}

public class AminoAcid {
	private char[]  sequence;
	private char    aminoacid;
	private boolean stop = false;
	
	
	
	public AminoAcid(char first, char second, char third) throws ApplicationException{
		System.out.println("AminoAcid Constructor("+first+","+second+","+third+")");
		sequence = new char[3];
		sequence[0] = first;
		sequence[1] = second;
		sequence[2] = third;
		try{
				aminoacid = TableOfStandardGeneticCode.getTable().getTranslation(sequence);
		}catch( AminoacidStopSequenceException e ){
			stop = true;
		}catch( AminoacidInvalidSequenceException e ){
			throw new ApplicationException("Aminoacid letter incorrect in '"+(new String(sequence))+"'");
		}catch (AminoacidNotFoundException e) {
			throw new ApplicationException("Aminoacid not found for sequence '"+(new String(sequence))+"'");
		}
	}
	public char get_aminoacid(){
		return aminoacid;
	}
	public char[] get_sequence(){
		return sequence;
	}
	public boolean isStop(){
		return stop;
	}
}

class TableOfStandardGeneticCode{
	private static Hashtable<String, String> table; 
	private static Hashtable possibleValues; 
	private static TableOfStandardGeneticCode singletonObject;
	private TableOfStandardGeneticCode(){
		System.out.println("TableOfStandardGeneticCode Constructor");
		possibleValues = new Hashtable();
		possibleValues.put('T', 1);
		possibleValues.put('A', 1);
		possibleValues.put('G', 1);
		possibleValues.put('C', 1);
		table = new Hashtable<String, String>();
		table.put("TTT", "F");
		table.put("TTC", "F");
		table.put("TTA", "L");
		table.put("TTG", "L");
		table.put("TCT", "S");
		table.put("TCC", "S");
		table.put("TCA", "S");
		table.put("TCG", "S");
		table.put("TAT", "Y");
		table.put("TAC", "Y");
		table.put("TAA", "STOP");//Termination
		table.put("TAG", "STOP");//Termination
		table.put("TGT", "C");
		table.put("TGC", "C");
		table.put("TGA", "STOP");//Termination
		table.put("TGG", "W");
		
		table.put("CTT", "L");
		table.put("CTC", "L");
		table.put("CTA", "L");
		table.put("CTG", "L");
		table.put("CCT", "P");
		table.put("CCC", "P");
		table.put("CCA", "P");
		table.put("CCG", "P");
		table.put("CAT", "H");
		table.put("CAC", "H");
		table.put("CAA", "Q");
		table.put("CAG", "Q");
		table.put("CGT", "R");
		table.put("CGC", "R");
		table.put("CGA", "R");
		table.put("CGG", "R");
		
		table.put("ATT", "I");
		table.put("ATC", "I");
		table.put("ATA", "I");
		table.put("ATG", "M");
		table.put("ACT", "T");
		table.put("ACC", "T");
		table.put("ACA", "T");
		table.put("ACG", "T");
		table.put("AAT", "N");
		table.put("AAC", "N");
		table.put("AAA", "K");
		table.put("AAG", "K");
		table.put("AGT", "S");
		table.put("AGC", "S");
		table.put("AGA", "R");
		table.put("AGG", "R");
		
		table.put("GTT", "V");
		table.put("GTC", "V");
		table.put("GTA", "V");
		table.put("GTG", "V");
		table.put("GCT", "A");
		table.put("GCC", "A");
		table.put("GCA", "A");
		table.put("GCG", "A");
		table.put("GAT", "D");
		table.put("GAC", "D");
		table.put("GAA", "E");
		table.put("GAG", "E");
		table.put("GGT", "G");
		table.put("GGC", "G");
		table.put("GGA", "G");
		table.put("GGG", "G");
	}
	
	public char getTranslation( char[] sequence ) throws AminoacidNotFoundException, AminoacidStopSequenceException, AminoacidInvalidSequenceException{
		String seq = new String(sequence);
		for( int i = 0 ; i < seq.length() ; i++ ){
			if( !possibleValues.containsKey(seq.charAt(i)) ){
				throw new AminoacidInvalidSequenceException();
			}
		}
		if( table.containsKey(seq) ){
			if( table.get(seq).equals("STOP")){
				throw new AminoacidStopSequenceException();
			}
			return table.get(seq).charAt(0);
		}
		throw new AminoacidNotFoundException();
	}
	public static TableOfStandardGeneticCode getTable(){
		if( null == singletonObject ){
			singletonObject = new TableOfStandardGeneticCode();
		}
		return singletonObject;
	}
	
}