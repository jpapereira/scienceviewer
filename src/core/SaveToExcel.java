package core;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Hashtable;
import java.util.Map.Entry;

import locale.Language;
import core.excel.*;

public class SaveToExcel {
	Hashtable<String,String> dictyDbResults;
	private Hashtable<String,ArrayList<Character>> data;
	public static void save(File file,Hashtable<String,ArrayList<Character>> data, Hashtable<String,String> dictyDbResults ) throws IOException{
		
		ExcelFile myFile = new ExcelFile(0);
		myFile.setFile(file);
		save( myFile , data, dictyDbResults );
	}
	public static void save(String filename,Hashtable<String,ArrayList<Character>> data, Hashtable<String,String> dictyDbResults ) throws IOException{
		
		ExcelFile file = new ExcelFile(0);
		file.setFilename(filename);
		save( file , data, dictyDbResults );
	}
	private static void save(ExcelFile file, Hashtable<String,ArrayList<Character>> data, Hashtable<String,String> dictyDbResults ) throws IOException{
		
		ExcelSheet sheet = new ExcelSheet(0);
		sheet.setName(Language.lang().getTranslation("excel.mainsheet"));
		file.addSheet(0, sheet);
		int numCols = 0;
		ArrayList<String> keys = new ArrayList<String>(data.keySet());
		Collections.sort(keys);
		numCols = data.get(keys.get(0)).size();
		
		
		for( int i = 0 ; i < numCols; i++ )
			sheet.addValue(0, i+1, Language.lang().getTranslation("viewer.panels.excelmatrix.columnhead")+ " " + (i));
		
		int rowNum = 1;
		for( String param:keys ){
			sheet.addValue(rowNum, 0, param);
			for( int i = 0 ; i < data.get(param).size(); i++ )
				sheet.addValue(rowNum, i+1, data.get(param).get(i).toString());
			rowNum++;
		}
		
		int sheetId = 1;
		for(String param : keys ){
			ExcelSheet auxSheet = new ExcelSheet(sheetId++);
			auxSheet.setName(Language.lang().getTranslation("excel.secondarysheet")+" "+param);
			auxSheet.addValue(0, 0, dictyDbResults.get(param));
			file.addSheet(sheetId-1, auxSheet);
			
		}
		
		ExcelLoader loader = new ExcelLoader();
		loader.createXmlFile(file);
	}
}
