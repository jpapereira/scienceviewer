package core;

import java.util.Hashtable;

public class Dna {
	private String struct;
	private AminoAcid[] aminoacidTranslation;
	private int numAminoacids;
	public Dna( String dnaStructure ) throws ApplicationException{
		System.out.println("DNA Constructor");
		this.struct = dnaStructure.toUpperCase();
		numAminoacids = (int)Math.ceil(this.struct.length()/3);
		aminoacidTranslation = new AminoAcid[numAminoacids];
		for( int i = 0 ; i < numAminoacids; i++){

			try {
				aminoacidTranslation[i] = new AminoAcid(this.struct.charAt(i*3),this.struct.charAt(i*3+1),this.struct.charAt(i*3+2));
			} catch (ApplicationException e) {
				// TODO Auto-generated catch block
				throw new ApplicationException(e.getMessage()+" starting in character "+(i*3)+" ");
			}
		}
		
	}
	
	public boolean compare( Dna toCompare){
		boolean result = true;
		if( this.struct.length() > toCompare.struct.length() ){
			MessageQueue.getQueue().addMessage("The length of the DNA is different Compared is smaller!");
			result = false;
		}else if( this.struct.length() < toCompare.struct.length() ){
			MessageQueue.getQueue().addMessage("The length of the DNA is different Compared is larger!");
			result = false;
		}
		int max = this.struct.length();
		if(toCompare.struct.length() < max )
			max = toCompare.struct.length();
		for( int i = 0 ; i < max ; i++ ){
			if( this.struct.charAt(i) != toCompare.struct.charAt(i) ){
				MessageQueue.getQueue().addMessage("Sequence incorrect at position: "+i);
				result = false;
			}
			System.out.println("Index is: "+i);
		}
		return result;
	}
	
	public String getAminoacidString(){
		String result = new String();
		for( int i =0 ; i < numAminoacids; i++ ){
			result = result + this.aminoacidTranslation[i].get_aminoacid();
		}
		return result;
	}
	
	
	public static boolean checkDNASequence( String sequence, boolean failOnError ) throws ApplicationException{
		int numAminoacids = (int)Math.ceil(sequence.length()/3);
		AminoAcid aminoacidTranslation;
		for( int i = 0 ; i < numAminoacids; i++){

			try {
				aminoacidTranslation = new AminoAcid(sequence.charAt(i*3),sequence.charAt(i*3+1),sequence.charAt(i*3+2));
			} catch (ApplicationException e) {
				// TODO Auto-generated catch block
				if( failOnError )
						throw new ApplicationException(e.getMessage()+" starting in character "+(i*3)+" ");
				else
					MessageQueue.getQueue().addMessage("Sequence incorrect at position: "+i);
			}
		}
		return true;
	}
	
	
	// A == T
	// T == A
	// C == G
	// G == C
	public static String antiparallel( String sequence ) throws ApplicationException{
		System.out.println("Sequence="+sequence);
		Hashtable antip = new Hashtable();
		antip.put('A', 'T');
		antip.put('T', 'A');
		antip.put('G', 'C');
		antip.put('C', 'G');
		
		String result = new String("");
		String names = sequence.toUpperCase();
		for( int i = (sequence.length()-1) ; i >= 0 ; i-- ){
			System.out.println("i="+i);
			result += antip.get(names.charAt(i));
			if( null == antip.get(names.charAt(i)) ){
				throw new ApplicationException("Sequence can only contain [ATGC] characters");
			}
		}
		
		
		
		return result;
	}
	public static String complement( String sequence ) throws ApplicationException{
		System.out.println("Sequence="+sequence);
		Hashtable antip = new Hashtable();
		antip.put('A', 'T');
		antip.put('T', 'A');
		antip.put('G', 'C');
		antip.put('C', 'G');
		
		String result = new String("");
		String names = sequence.toUpperCase();
		for( int i = 0 ; i < names.length() ; i++ ){
			System.out.println("i="+i);
			result += antip.get(names.charAt(i));
			if( null == antip.get(names.charAt(i)) ){
				throw new ApplicationException("Sequence can only contain [ATGC] characters");
			}
		}
		
		
		return result;
	}
	public static String invert( String sequence ){
		StringBuffer a = new StringBuffer( sequence );
		return a.reverse().toString().toUpperCase();
	}
	
}
