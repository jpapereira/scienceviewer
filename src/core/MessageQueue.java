/**
 * 
 */
package core;

/**
 * @author bubu
 *
 */
public class MessageQueue {
	
	private static MessageQueue singletonObj;
	
	private static String[] messages;
	private int actualIndex = -1;
	
	private static final int MESSAGE_QUEUE_SIZE = 100;
	
	public static MessageQueue getQueue(){
		if( null == singletonObj ){
			singletonObj = new MessageQueue();
		}
		
		return singletonObj;
	}
	
	
	public boolean addMessage(String message){
		
		if(MESSAGE_QUEUE_SIZE <= (actualIndex+1) )
			return false;
		actualIndex++;
		messages[actualIndex] = message;
		
		
		return true;
	}
	
	public String getMessage(){
		if( 0 > actualIndex )
			return null;
		String result = messages[actualIndex];
		messages[actualIndex] = null;
		actualIndex--;
		return result;
	}
	
	public int getNumberMessages(){
		return actualIndex + 1;
	}
	
	public void clearQueue(){
		for( int i = 0 ; i <= actualIndex ; i++){
			messages[i] = null;
		}
		actualIndex = -1;
	}
	
	public void printQueue(){
		for( int i = 0 ; i < actualIndex; i++){
			System.out.println(messages[i]);
		}
	}
	
	
	private MessageQueue(){
		messages = new String[MESSAGE_QUEUE_SIZE];
		actualIndex = 0;
	}
	
}
