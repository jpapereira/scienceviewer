package core.excel;

import java.util.Map;
import java.util.TreeMap;


public class ExcelSheet {
	private TreeMap<Integer,ExcelColumn> table;
	private int idx;
	private String name;
	public ExcelSheet( int idx ){
		table = new TreeMap<Integer,ExcelColumn>();
		this.idx = idx;
		setName(new String());
	}
	public TreeMap<Integer,ExcelColumn> getSheet(){
		return table;
	}
	public ExcelColumn getColumn( int index ){
		return table.get(index);
	}
	public String getValue(int row, int column){
		return table.get(column).getValue(row);
	}
	public void addValue( int row, int column, String value){
		//System.out.println("Adding value ["+column+"]["+row+"]:["+value+"]");
		ExcelColumn r;
		r = table.get(column);
		if( null == r ){
			//System.out.println("Created a new Column");
			r = new ExcelColumn( column );
			table.put(column, r);
		}
		
		r.addValue(row, value);
		
	}
	public boolean isPresent( int row, int column, String toCompare ){
		if( column == -1 ){
			//System.out.println("Looking on ALL column ");
			for( Map.Entry<Integer,ExcelColumn> r: table.entrySet() ){
				if( r.getValue().isPresent(column, toCompare))
					return true;
			}
		}else{
			//System.out.println("Looking on column "+column);
			return table.get(column).isPresent( row, toCompare );
		}
		return false;
	}
	public void print(){
		System.out.println("\tStart sheet: "+idx+" num columns:"+table.size());
		for( Map.Entry<Integer,ExcelColumn> r: table.entrySet() )
			r.getValue().print();
		System.out.println("\tEnd sheet: "+idx);
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
}
