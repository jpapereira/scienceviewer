package core.excel;

public class ExcelRow{
	private String value;
	private int idx;
	public ExcelRow( int idx, String value){
		this.setIndex(idx);
		this.setValue(value);
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public int getIndex() {
		return idx;
	}
	public void setIndex(int idx) {
		this.idx = idx;
	}
}