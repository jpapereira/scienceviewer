package core.excel;

import java.io.File;
import java.util.Map;
import java.util.TreeMap;


public class ExcelFile {
	private TreeMap<Integer,ExcelSheet> table;
	private int idx;
	private int entries = 0;
	private String filename;
	private File file;
	public ExcelFile( int id ){
		table = new TreeMap<Integer,ExcelSheet>();
		idx = id;
		setFilename(new String());
	}
	public int retrieveIndex(){
		return idx;
	}
	public TreeMap<Integer,ExcelSheet> getFile(){
		return table;
	}
	
	public void addSheet( int position,  ExcelSheet sheet){
		table.put(position, sheet);
	}
	public ExcelSheet getSheet( int index ){
		return table.get(index);
	}
	public String getValue( int sheet, int row, int column ){
		return table.get(sheet).getValue( row , column );
	}
	public void addValue( int sheet, int row, int column, String value ){
		//System.out.println("Adding value  ["+idx+"]["+sheet+"]["+column+"]["+row+"]:["+value+"]");
		entries++;
		ExcelSheet r;
		r = table.get(sheet);
		if( null == r ){
			System.out.println("Created a new Sheet");
			r = new ExcelSheet(sheet);
			table.put(row, r);
		}
		
		r.addValue(row, column, value);
		
	}
	public int getMaxRow( int  sheet ){
		return table.get(sheet).getSheet().size();
	}
	public boolean isPresent( int sheet, int row, int column, String toCompare ){
		if( -1 == sheet ){
			for( Map.Entry<Integer,ExcelSheet> sheet1: table.entrySet() )
				if( sheet1.getValue().isPresent( row , column , toCompare) )
					return true;
		}else{
			//System.out.println("Looking on sheet "+sheet);
			return table.get(sheet).isPresent( row, column, toCompare);
		}
		return false;
	}
	public void print(){
		System.out.println("File Start "+idx+" as "+table.size()+" Sheets");
		for( Map.Entry<Integer,ExcelSheet> sheet1: table.entrySet() )
			sheet1.getValue().print();
		System.out.println("File End");
	}
	public int getEntries( ){
		return entries;
	}
	/**
	 * @return the filename
	 */
	public String getFilename() {
		return filename;
	}
	/**
	 * @param filename the filename to set
	 */
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public void setFile(File file ){
		this.file = file;
	}
	public File getFileObj(){
		return file;
	}
}
