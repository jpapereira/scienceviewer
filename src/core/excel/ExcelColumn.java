package core.excel;

import java.util.Map;
import java.util.TreeMap;


public class  ExcelColumn{
	private TreeMap<Integer,ExcelRow> column;
	private int idx;
	public ExcelColumn(int idx ){
		setColumn(new TreeMap<Integer,ExcelRow>());
		this.idx = idx;
	}
	public TreeMap<Integer,ExcelRow> getColumn() {
		return column;
	}
	public ExcelRow getRow( int index ){
		return column.get(index);
	}
	public void setColumn(TreeMap<Integer,ExcelRow> column) {
		this.column = column;
	}
	public String getValue( int row ){
		return column.get(row).getValue();
	}
	public void addValue( int row, String value){
		//System.out.println("Adding value ["+row+"]:["+value+"]");
		column.put(row, new ExcelRow( row, value));
	}
	public boolean isPresent( int row, String toCompare){
		if( row == -1 ){
			//System.out.println("Looking on all rows ");
			for( Map.Entry<Integer,ExcelRow> c: column.entrySet()){
				//System.out.println("Checking against: "+c.getValue().getValue());
				if( c.getValue().getValue().equals(toCompare))
					return true;
			}
		}else{
			//System.out.println("Looking on row "+row);
			return column.get(row).getValue() == toCompare;
		}
		return false;
	}
	public void print(){
		System.out.println("\t\tStart column: "+idx+" num rows:"+column.size());
		for( Map.Entry<Integer,ExcelRow> r: column.entrySet() )
			System.out.println("\t\t\tvalue: "+r.getValue().getValue());
		System.out.println("\t\tEnd column: "+idx);
	}
}