package core;

import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.SocketAddress;

public class Context {
	private static Context instance = null;
	private Proxy proxy;
	
	private Float waitingFor = (float)0;
	private Float actually = (float) 0;
	
	private Context(){
		proxy = null;
	}
	public static Context instance(){
		if( instance == null){
			synchronized( Context.class ){
				if( instance == null )
					instance = new Context();
			}
		}
		return instance;
	}
	public Proxy getProxy(){
		return proxy;
	}
	public void setProxy( String address, int port){
		SocketAddress addr = new
				InetSocketAddress(address, port);
		proxy = new Proxy(Proxy.Type.HTTP, addr);
	}
	public void setProxy( ){
		proxy = null;
	}
	
	public void waitingFor( int num ){
		System.out.println("Waiting for :"+num);
		waitingFor = (float) num;
		synchronized( actually ){
			actually = (float) 0;
		}
	}
	public float percentageDone(){
		
		return (actually/waitingFor)*100;
	}
	public void workDone( ){
		synchronized( this.actually ){
			this.actually++;
		}
		System.out.println("Actually on :"+actually);
	}
	
}
