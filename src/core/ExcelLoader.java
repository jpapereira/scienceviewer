/**
 * 
 */
package core;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.TreeMap;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.CellReference;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

import core.excel.*;

import viewer.main.ProgressBar;

/**
 * @author bluetc-1205
 *
 */
public class ExcelLoader {
	private File[] allFiles;
	private ArrayList<TreeMap<String,TreeMap<String,Integer>>> res;
	private ProgressBar bar;
	private ArrayList<ExcelFile> decodedFiles;
	public ExcelLoader(){
		allFiles = new File[50];
		res = new ArrayList<TreeMap<String,TreeMap<String,Integer>>>();
		decodedFiles = new ArrayList<ExcelFile>();
		bar = null;
	}
	public ExcelLoader(File [] compare, ProgressBar bar, int factor){
		allFiles = compare;
		
		res = new ArrayList<TreeMap<String,TreeMap<String,Integer>>>();
		decodedFiles = new ArrayList<ExcelFile>();
		this.bar = bar;
		
		loadAll();
		
	}
	private void loadAll(){
		int i = 0;
		for( File file : allFiles){
			try {
				System.out.println("loadAll: "+i);
				if( allFiles[i] == null )
					break;
				decodedFiles.add(i, new ExcelFile(i));
				System.out.println("loadAll: "+i);
				FileInputStream f = new FileInputStream(allFiles[i]);
				//Get the workbook instance for XLS file 
				HSSFWorkbook workbook = new HSSFWorkbook(f);
				//res.add(i, new TreeMap<String,TreeMap<String,Integer>>());
				 
				Sheet sheet1 = workbook.getSheetAt(0);
			    for (Row row : sheet1) {
			    	
			        for (Cell cell : row) {
			            CellReference cellRef = new CellReference(row.getRowNum(), cell.getColumnIndex());
			            if( cell.getColumnIndex() > 0)
			            	break;
			            
			            
			            //System.out.print(cellRef.formatAsString());
			            //System.out.print(" - ");

			            switch (cell.getCellType()) {
			                case Cell.CELL_TYPE_NUMERIC:
			                    if (!DateUtil.isCellDateFormatted(cell)) {
/*			                        System.out.println(cell.getNumericCellValue());
			                        TreeMap<String,Integer> aux;
			                        try{
				                        aux = res.get(i).get(cell.getColumnIndex());				                        
			                        	System.out.println("Add");
			                        	aux.put((new String(""+cell.getNumericCellValue())), 1);
			                        }catch( NullPointerException e){
			                        	System.out.println("New");
			                        	aux = new TreeMap<String,Integer>();
			                        	aux.put((new String(""+cell.getNumericCellValue())), 1);
			                 
			                        	res.get(i).put((new String("")+cell.getColumnIndex()),aux);
			                        }*/
			                    	System.out.println(new String(""+cell.getNumericCellValue()));
			                        decodedFiles.get(i).addValue(0, row.getRowNum() , cell.getColumnIndex(), new String(""+cell.getNumericCellValue()) );
			                    }
			                    break;
			                case Cell.CELL_TYPE_STRING:
			                	decodedFiles.get(i).addValue(0, row.getRowNum() , cell.getColumnIndex(), cell.getStringCellValue() );
			                	break;
			            }
			        }
			    }
				f.close();
				
				 
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			i++;
		}
	}
	public void createXmlFile( ExcelFile file ) throws IOException{
		HSSFWorkbook workbook = new HSSFWorkbook();
		ExcelSheet wSheet;
		ExcelColumn wColumn;
		ExcelRow wRow;
		for( int sheetNum : file.getFile().keySet() ){
			wSheet = file.getFile().get(sheetNum);
			System.out.println(wSheet.getName());
			HSSFSheet sheet = workbook.createSheet(wSheet.getName());
			Row row = null;
			for( int columnNum : wSheet.getSheet().keySet() ){
				wColumn = wSheet.getSheet().get(columnNum);
				for( int rowNum: wColumn.getColumn().keySet()){
					if( null == (row= sheet.getRow(rowNum) ) )
						row = sheet.createRow(rowNum);
					Cell cell = row.createCell(columnNum);
					cell.setCellValue(wColumn.getValue(rowNum));
						
				}
			}
		}
		
/*		HSSFSheet sheet = workbook.createSheet("Sample sheet");
		//Create a new row in current sheet
		Row row = sheet.createRow(0);
		//Create a new cell in current row
		Cell cell = row.createCell(0);
		//Set value to new value
		cell.setCellValue("Blahblah");*/
		FileOutputStream out;
		
		if( null == file.getFile())
			out = new FileOutputStream(new File(file.getFilename()));
		else
			out = new FileOutputStream(file.getFileObj());
		workbook.write(out);
	    out.close();
	
	    System.out.println("Excel written successfully..");
	}
	
	public ArrayList<ExcelFile> retrieveFilesLoaded(){
		return decodedFiles;
	}
}
