package core;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.TreeMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.CellReference;
import org.apache.poi.ss.usermodel.*;

import core.excel.*;

import viewer.main.ProgressBar;
public class CompareExcel {
	
	public enum CompareType{
		PRESENT_REFERENCE,
		NOT_PRESENT_REFERENCE,
		PRESENT_ON_BY_ONE,
		NOT_PRESENT_ON_BY_ONE
	};
	
	private int ref;
	private File[] allFiles;
	private ArrayList<ArrayList<ArrayList<String>>> results;
	private ArrayList<TreeMap<String,TreeMap<String,Integer>>> res;
	private ArrayList<ArrayList<String>> foundStuff;
	private ProgressBar bar;
	private int barFactor;
	
	private int maxFileColumn = 0;
	private int maxData = 0;
	
	private ArrayList<ExcelFile> decodedFiles;
	
	
	
	public CompareExcel(File [] compare, int reference, ProgressBar bar, int factor){
		allFiles = compare;
		ref = reference;
		res = new ArrayList<TreeMap<String,TreeMap<String,Integer>>>();
		results = new ArrayList<ArrayList<ArrayList<String>>>();
		foundStuff = new ArrayList<ArrayList<String>>();
		
		ExcelLoader loader = new ExcelLoader(compare,bar,factor);
		
		decodedFiles = loader.retrieveFilesLoaded();
		this.bar = bar;
		barFactor = factor;
		
		
	}
	
	
	public Hashtable<String,ArrayList<Character>> retrieveMatrix(){
		Hashtable<String,ArrayList<Character>> result = new Hashtable<String,ArrayList<Character>>();
		for( ExcelFile file: decodedFiles ){
			for( Entry<Integer,ExcelSheet> msheet : file.getFile().entrySet() ){
				for( Entry<Integer,ExcelColumn> actCol : msheet.getValue().getSheet().entrySet() ){
					for( Entry<Integer,ExcelRow> col: actCol.getValue().getColumn().entrySet()){
						if( result.containsKey(col.getValue().getValue()) ){
							result.get(col.getValue().getValue()).set(file.retrieveIndex(),'X');
						}else{
							result.put(col.getValue().getValue(), new ArrayList<Character>());
							for( ExcelFile f: decodedFiles ){
								if( f == file )
									result.get(col.getValue().getValue()).add('X');
								else
									result.get(col.getValue().getValue()).add(' ');
							}
								
						}
					}
				}
			}
		}
		return result;
	}
	 
	
	public ArrayList<ArrayList<String>> compare( CompareExcel.CompareType type , int sheet, int row, int column ){
		ArrayList<ArrayList<String>> foundStuff = new ArrayList<ArrayList<String>>();
		boolean compare = true;
		if( type == CompareExcel.CompareType.PRESENT_REFERENCE )
			compare = true;
		else if(type == CompareExcel.CompareType.NOT_PRESENT_REFERENCE)
			compare = false;
		else if(type == CompareExcel.CompareType.NOT_PRESENT_ON_BY_ONE)
			compare = false;
		
		int barProgresInc = 0;
		
		
		for(ExcelFile file :decodedFiles ){
			System.out.println("New file!");
			foundStuff.add(decodedFiles.indexOf(file), new ArrayList<String>());
			barProgresInc++;
			bar.increment(barFactor);
			if( file == decodedFiles.get(ref)){
				System.out.println("Reference");
				ExcelColumn c = file.getFile().get(sheet).getSheet().get(column);
				for( Map.Entry<Integer,ExcelRow> col: c.getColumn().entrySet()){
					foundStuff.get(ref).add(col.getValue().getValue());
				}
				continue;
			}
			//System.out.println("Not Reference");
			for( Entry<Integer,ExcelSheet> msheet : file.getFile().entrySet() ){
				//System.out.println("Entry");
				for( Entry<Integer,ExcelColumn> actCol : msheet.getValue().getSheet().entrySet() ){
					//System.out.println("Column");
					for( Entry<Integer,ExcelRow> col: actCol.getValue().getColumn().entrySet()){
						System.out.println("Row: "+col.getValue().getValue());
						boolean isPresent =decodedFiles.get(ref).isPresent(sheet, row, column, col.getValue().getValue() ) ; 
						if( isPresent && compare){
							System.out.println("Found");
							foundStuff.get(decodedFiles.indexOf(file)).add(col.getValue().getValue());
						}else if( !isPresent && !compare ){
							System.out.println("Not Found");
							foundStuff.get(decodedFiles.indexOf(file)).add(col.getValue().getValue());
						}
							
					}
				}
			}
		}
		
		return foundStuff;
/*		if( type == 0 ){
			int i = 0;
			for( TreeMap<String,TreeMap<String,Integer>> file: res ){
				System.out.println("New file");
				if( file != res.get(ref) ){
					foundStuff.add(i, new ArrayList<String>());
					
					for( Entry<String, TreeMap<String, Integer>> row1: file.entrySet() ){
						System.out.println("row: "+row1.getKey());
						for( Entry<String, Integer> value : row1.getValue().entrySet() ){
							if( res.get(ref).containsKey(value.getKey()) ){
								System.out.println("Add to existing");
								foundStuff.get(i).add(value.getKey());
							}
						}
						
					}
				}
				i++;
			}
			*/
			
/*			int i = 0;
			for(TreeMap<String,Integer> it : this.res){
				System.out.println("Piflas: "+i);
				System.out.println("asd:"+it.keys().toString());
				if( it != res.get(ref)){
					for( String a: it.keySet() ){
						System.out.println("Search for:"+a);
						if( res.get(ref).containsKey(a)){
							System.out.println("found it");
							if( foundStuff.contains(i) ){
								System.out.println("Add to existing");
								foundStuff.get(i).add(a);
							}else{
								System.out.println("Add to new");
								ArrayList<String> tmp = new ArrayList<String>();
								tmp.add(a);
								foundStuff.add(tmp);
							}
						}
					}
				}
				i++;
			}*/
		/*}
		return foundStuff;*/
		
	}
	
	public void print(){
		for( ExcelFile file : decodedFiles )
			file.print();
	}
	
}

