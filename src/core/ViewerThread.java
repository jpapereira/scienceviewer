package core;

public abstract class ViewerThread extends Thread {
	private volatile boolean keepWorking = true;
	
	protected int sleep = 10;
	
	public void run(){
		while( keepWorking ){
			try{
				work();
				Thread.sleep(sleep);
			}catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	protected abstract void work();
		
	public void stopThread(){
		keepWorking = false;
		this.interrupt();
	}
}
