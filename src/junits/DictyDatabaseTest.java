/**
 * 
 */
package junits;

import static org.junit.Assert.*;

import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.SocketAddress;

import org.junit.Assert;
import org.junit.Test;

import core.DictyDatabase;

/**
 * @author bluetc-1205
 *
 */
public class DictyDatabaseTest {

	/**
	 * Test method for {@link core.DictyDatabase#DictyDatabase(java.net.Proxy)}.
	 */
	@Test
	public final void testDictyDatabase() {
		fail("Not yet implemented"); // TODO
	}
	
	/**
	 * Test method for {@link core.DictyDatabase#parse(java.lang.String)}.
	 * @throws Exception 
	 */
	@Test
	public final void testParse() throws Exception {
		DictyDatabase db = new DictyDatabase(null);
		
		Assert.assertEquals(new String("pico"), db.parse("<html><pre>pico</pre></html>") );
		Assert.assertEquals(new String("pico"), db.parse("<html><prepare><pre>pico</pre></html>") );
		String expected = new String(">DDB0185178|DDB_G0292390 |Protein|gene: atg1 on chromosome: 6 position 1694801 to 1697129"+
"MKRVGDYILDKRIGWGAFAQVYKGFSIKTNEPFAIKVVDVCRLADKNSKLTENLNYEIRI"+
"LKELSHTNIVRLYDVLNEETDPTFIYMIMECCEGGDFSKYIRTHKKLTEEKALYFMKQLA"+
"NGLKFLRQKQIVHRDLKPQNLLLSDDSEHPILKIGDFGFAKFIDPFSLSDTFCGSPLYMA"+
"PEILHRKNYTVKADLWSVGIILYEMLVGEPAYNSGSVPDLLNQLQNKKIKLPSHISSDCQ"+
"NLIYSLLQIDVEKRISWEDFFNHKWLNLNNNDSYKNNSGNYFNNNNINNNNNNNTNNNNN"+
"NISYPISINSNNTNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNYYNNNNSPPNVY"+
"HASSLPYDFNNNNNNNNSNNYNNNTNVPNSLPFAYNNNIYSSPTEAIPQPTTLNKSKSLE"+
"NTGNTIRAHPFKDDKKSTTIQQPQQQQQQQQQQQQQQQQQQQQQQQQQQQNRQLSNLSTD"+
"FERDLVILDGEELESMERVFNRAVAIAELGDLRQNEPLECVPLYILALKLMKSKIPNDPS"+
"SSPDKFINTFTEYKRKLVHIFSTSNTSVKNQDHHSSFSPNRFIYENALEFGKKGAVEELY"+
"NNYPTSLQFYTDGTLLLEYLSSIVIDSDDQEIIKKYLNAFEVRTQICKKNYENSKNTVLN"+
"TNSIQNNT*");
		
		String page = new String("<html><head><title> <title>DDB0185178 Protein sequence</title></head><body><pre>>DDB0185178|DDB_G0292390 |Protein|gene: atg1 on chromosome: 6 position 1694801 to 1697129"+
				"MKRVGDYILDKRIGWGAFAQVYKGFSIKTNEPFAIKVVDVCRLADKNSKLTENLNYEIRI"+
				"LKELSHTNIVRLYDVLNEETDPTFIYMIMECCEGGDFSKYIRTHKKLTEEKALYFMKQLA"+
				"NGLKFLRQKQIVHRDLKPQNLLLSDDSEHPILKIGDFGFAKFIDPFSLSDTFCGSPLYMA"+
				"PEILHRKNYTVKADLWSVGIILYEMLVGEPAYNSGSVPDLLNQLQNKKIKLPSHISSDCQ"+
				"NLIYSLLQIDVEKRISWEDFFNHKWLNLNNNDSYKNNSGNYFNNNNINNNNNNNTNNNNN"+
				"NISYPISINSNNTNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNYYNNNNSPPNVY"+
				"HASSLPYDFNNNNNNNNSNNYNNNTNVPNSLPFAYNNNIYSSPTEAIPQPTTLNKSKSLE"+
				"NTGNTIRAHPFKDDKKSTTIQQPQQQQQQQQQQQQQQQQQQQQQQQQQQQNRQLSNLSTD"+
				"FERDLVILDGEELESMERVFNRAVAIAELGDLRQNEPLECVPLYILALKLMKSKIPNDPS"+
				"SSPDKFINTFTEYKRKLVHIFSTSNTSVKNQDHHSSFSPNRFIYENALEFGKKGAVEELY"+
				"NNYPTSLQFYTDGTLLLEYLSSIVIDSDDQEIIKKYLNAFEVRTQICKKNYENSKNTVLN"+
				"TNSIQNNT*</pre></body></html>");
		Assert.assertEquals(expected, db.parse(page) );
		
	}


	/**
	 * Test method for {@link core.DictyDatabase#search(core.DictyDatabase.SequenceType, java.lang.String)}.
	 * @throws Exception 
	 */
	@Test
	public final void testSearch() throws Exception {
		SocketAddress addr = new
				InetSocketAddress("localhost", 8080);
		Proxy proxy = new Proxy(Proxy.Type.HTTP, addr);
		DictyDatabase db = new DictyDatabase(proxy);
		
		
		String expected = new String(">DDB0185178|DDB_G0292390 |Protein|gene: atg1 on chromosome: 6 position 1694801 to 1697129"+
				"MKRVGDYILDKRIGWGAFAQVYKGFSIKTNEPFAIKVVDVCRLADKNSKLTENLNYEIRI"+
				"LKELSHTNIVRLYDVLNEETDPTFIYMIMECCEGGDFSKYIRTHKKLTEEKALYFMKQLA"+
				"NGLKFLRQKQIVHRDLKPQNLLLSDDSEHPILKIGDFGFAKFIDPFSLSDTFCGSPLYMA"+
				"PEILHRKNYTVKADLWSVGIILYEMLVGEPAYNSGSVPDLLNQLQNKKIKLPSHISSDCQ"+
				"NLIYSLLQIDVEKRISWEDFFNHKWLNLNNNDSYKNNSGNYFNNNNINNNNNNNTNNNNN"+
				"NISYPISINSNNTNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNYYNNNNSPPNVY"+
				"HASSLPYDFNNNNNNNNSNNYNNNTNVPNSLPFAYNNNIYSSPTEAIPQPTTLNKSKSLE"+
				"NTGNTIRAHPFKDDKKSTTIQQPQQQQQQQQQQQQQQQQQQQQQQQQQQQNRQLSNLSTD"+
				"FERDLVILDGEELESMERVFNRAVAIAELGDLRQNEPLECVPLYILALKLMKSKIPNDPS"+
				"SSPDKFINTFTEYKRKLVHIFSTSNTSVKNQDHHSSFSPNRFIYENALEFGKKGAVEELY"+
				"NNYPTSLQFYTDGTLLLEYLSSIVIDSDDQEIIKKYLNAFEVRTQICKKNYENSKNTVLN"+
				"TNSIQNNT*"+((char)1));
		
		
		db.search(DictyDatabase.SequenceType.PROTEIN, "DDB0185178");
		String result = db.retrieveSearch( "DDB0185178");
		int i = 0;
		for( char a: result.toCharArray()){
			System.out.println("Char at:"+i+" is :"+((int)a));
			System.out.println(expected.charAt(i)+"=="+a);
			if( ((int)a) > 65 && ((int)a) < 123 ){
				System.out.println("Test: "+((int)a));
				Assert.assertEquals(expected.charAt(i),result.charAt(i));
			}
			i++;
		}
		
	}
	/**
	 * Test method for {@link core.DictyDatabase#search(core.DictyDatabase.SequenceType, java.lang.String)}.
	 * @throws Exception 
	 */
	@Test
	public final void testMultipleSearch() throws Exception {
		SocketAddress addr = new
				InetSocketAddress("localhost", 8080);
		Proxy proxy = new Proxy(Proxy.Type.HTTP, addr);
		DictyDatabase db = new DictyDatabase(proxy);
		
		
		String expected = new String(">DDB0185178|DDB_G0292390 |Protein|gene: atg1 on chromosome: 6 position 1694801 to 1697129"+
				"MKRVGDYILDKRIGWGAFAQVYKGFSIKTNEPFAIKVVDVCRLADKNSKLTENLNYEIRI"+
				"LKELSHTNIVRLYDVLNEETDPTFIYMIMECCEGGDFSKYIRTHKKLTEEKALYFMKQLA"+
				"NGLKFLRQKQIVHRDLKPQNLLLSDDSEHPILKIGDFGFAKFIDPFSLSDTFCGSPLYMA"+
				"PEILHRKNYTVKADLWSVGIILYEMLVGEPAYNSGSVPDLLNQLQNKKIKLPSHISSDCQ"+
				"NLIYSLLQIDVEKRISWEDFFNHKWLNLNNNDSYKNNSGNYFNNNNINNNNNNNTNNNNN"+
				"NISYPISINSNNTNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNYYNNNNSPPNVY"+
				"HASSLPYDFNNNNNNNNSNNYNNNTNVPNSLPFAYNNNIYSSPTEAIPQPTTLNKSKSLE"+
				"NTGNTIRAHPFKDDKKSTTIQQPQQQQQQQQQQQQQQQQQQQQQQQQQQQNRQLSNLSTD"+
				"FERDLVILDGEELESMERVFNRAVAIAELGDLRQNEPLECVPLYILALKLMKSKIPNDPS"+
				"SSPDKFINTFTEYKRKLVHIFSTSNTSVKNQDHHSSFSPNRFIYENALEFGKKGAVEELY"+
				"NNYPTSLQFYTDGTLLLEYLSSIVIDSDDQEIIKKYLNAFEVRTQICKKNYENSKNTVLN"+
				"TNSIQNNT*"+((char)1));
		
		
		db.search(DictyDatabase.SequenceType.PROTEIN, "DDB0185178");
		db.search(DictyDatabase.SequenceType.PROTEIN, "DDB0185178");
		for( int i = 185000 ; i < 185300 ; i++ )
			db.search(DictyDatabase.SequenceType.PROTEIN, "DDB0"+i);
		db.search(DictyDatabase.SequenceType.PROTEIN, "DDB0185178");
		String result = db.retrieveSearch( "DDB0185178");
		String result1 = db.retrieveSearch( "DDB0185178");
		Assert.assertEquals(result,result1);
		int i = 0;
		for( char a: result.toCharArray()){
			System.out.println("Char at:"+i+" is :"+((int)a));
			System.out.println(expected.charAt(i)+"=="+a);
			if( ((int)a) > 65 && ((int)a) < 123 ){
				System.out.println("Test: "+((int)a));
				Assert.assertEquals(expected.charAt(i),result.charAt(i));
			}
			i++;
		}
		
	}

	
}
