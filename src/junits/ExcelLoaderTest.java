/**
 * 
 */
package junits;

import static org.junit.Assert.*;

import org.junit.Test;

import core.ExcelLoader;
import core.excel.*;

/**
 * @author bluetc-1205
 *
 */
public class ExcelLoaderTest {

	/**
	 * Test method for {@link core.ExcelLoader#createXmlFile(core.ExcelFile)}.
	 */
	@Test
	public final void testCreateXmlFile() {
		
		ExcelFile f = new ExcelFile(1);
		f.setFilename("D:\\result.xls");
		ExcelSheet sheet = new ExcelSheet(0);
		sheet.setName("bombaim");
		f.addSheet(0, sheet);
		sheet = new ExcelSheet(1);
		sheet.setName("q");
		f.addSheet(1, sheet);
		
		f.addValue(0, 10, 5, "batata 10-5");
		f.addValue(1, 6, 1, "batata 6-1");
		f.addValue(0, 0, 0, "batata 0-0");
		//f.addValue(0, 1, 1, "batata 1-1");
		
		ExcelLoader loader = new ExcelLoader();
		
		loader.createXmlFile(f);
		
		
	}

}
