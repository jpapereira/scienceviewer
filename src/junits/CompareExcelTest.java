/**
 * 
 */
package junits;

import static org.junit.Assert.*;

import java.io.File;
import java.util.ArrayList;
import java.util.Hashtable;

import junit.framework.Assert;

import org.junit.Test;

import viewer.main.ProgressBar;

import core.CompareExcel;

/**
 * @author bluetc-1205
 *
 */
public class CompareExcelTest {

	/**
	 * Test method for {@link core.CompareExcel#CompareExcel(java.io.File[], int)}.
	 */
	@Test
	public final void testCompareExcel() {
		File[] files = new File[3];
		files[0] = new File("C:\\Users\\bluetc-1205\\Desktop\\test.xls");
		files[1] = new File("C:\\Users\\bluetc-1205\\Desktop\\test1.xls");
		files[2] = new File("C:\\Users\\bluetc-1205\\Desktop\\test.xls");
		CompareExcel obj = new CompareExcel( files,0, new ProgressBar(), 100);
		Assert.assertNotNull(obj);
	}

	/**
	 * Test method for {@link core.CompareExcel#compare(int, int, int, int)}.
	 */
	@Test
	public final void testCompare() {
		File[] files = new File[3];
		files[0] = new File("C:\\Users\\bluetc-1205\\Desktop\\test.xls");
		files[1] = new File("C:\\Users\\bluetc-1205\\Desktop\\test1.xls");
		files[2] = new File("C:\\Users\\bluetc-1205\\Desktop\\test.xls");
		CompareExcel obj = new CompareExcel( files,0,new ProgressBar(), 100 );
		obj.print();
		ArrayList<ArrayList<String>> expected = new ArrayList<ArrayList<String>>();
		for( int i = 0 ; i < files.length; i++){
			expected.add(new ArrayList<String>());
			expected.get(i).add("1.0");
			expected.get(i).add("2.0");
			expected.get(i).add("4.0");
		}
		ArrayList<ArrayList<String>> result = obj.compare(CompareExcel.CompareType.PRESENT_REFERENCE, 0, -1, 0);
		Assert.assertEquals(expected, result);
		//fail("Not yet implemented"); // TODO
	}
	@Test
	public final void testCompareNotPresent() {
		System.out.println("============================================================");
		File[] files = new File[3];
		files[0] = new File("C:\\Users\\bluetc-1205\\Desktop\\test.xls");
		files[1] = new File("C:\\Users\\bluetc-1205\\Desktop\\test1.xls");
		files[2] = new File("C:\\Users\\bluetc-1205\\Desktop\\test.xls");
		CompareExcel obj = new CompareExcel( files,0,new ProgressBar(), 100 );

		ArrayList<ArrayList<String>> expected = obj.compare(CompareExcel.CompareType.PRESENT_REFERENCE, 0, -1, 0);
		System.out.println("%%");
		ArrayList<ArrayList<String>> result = obj.compare(CompareExcel.CompareType.NOT_PRESENT_REFERENCE, 0, -1, 0);
		System.out.println(expected.toString());
		System.out.println(result.toString());
		System.out.println("============================================================");
		Assert.assertNotSame(expected, result);
		//fail("Not yet implemented"); // TODO
	}@Test
	public final void testCompareNotPresentOneOnOne() {
		System.out.println("============================================================");
		File[] files = new File[3];
		File[] files1 = new File[2];
		files[0] = new File("C:\\Users\\bluetc-1205\\Desktop\\test.xls");
		files[1] = new File("C:\\Users\\bluetc-1205\\Desktop\\test1.xls");
		files[2] = new File("C:\\Users\\bluetc-1205\\Desktop\\test.xls");
		CompareExcel obj = new CompareExcel( files,0,new ProgressBar(), 100 );

		ArrayList<ArrayList<String>> expected = obj.compare(CompareExcel.CompareType.PRESENT_REFERENCE, 0, -1, 0);
		System.out.println("%%");
		
		files1[0] = new File("C:\\Users\\bluetc-1205\\Desktop\\test.xls");
		files1[1] = new File("C:\\Users\\bluetc-1205\\Desktop\\test1.xls"); 
		obj = new CompareExcel( files1,0,new ProgressBar(), 100 );
		ArrayList<ArrayList<String>> result = obj.compare(CompareExcel.CompareType.NOT_PRESENT_ON_BY_ONE, 0, -1, 0);
		
		files1[0] = new File("C:\\Users\\bluetc-1205\\Desktop\\test1.xls");
		files1[1] = new File("C:\\Users\\bluetc-1205\\Desktop\\test.xls");
		obj = new CompareExcel( files1,0,new ProgressBar(), 100 );
		ArrayList<ArrayList<String>> result1 = obj.compare(CompareExcel.CompareType.NOT_PRESENT_ON_BY_ONE, 0, -1, 0);
		System.out.println(expected.toString());
		System.out.println(result.toString());
		System.out.println(result1.toString());
		System.out.println("============================================================");
		Assert.assertEquals(result, result1);
		//fail("Not yet implemented"); // TODO
	}
	@Test
	public final void testMatrix() {
		System.out.println("============================================================");
		File[] files = new File[3];
		File[] files1 = new File[2];
		files[0] = new File("C:\\Users\\bluetc-1205\\Desktop\\test.xls");
		files[1] = new File("C:\\Users\\bluetc-1205\\Desktop\\test1.xls");
		files[2] = new File("C:\\Users\\bluetc-1205\\Desktop\\test.xls");
		CompareExcel obj = new CompareExcel( files,0,new ProgressBar(), 100 );

		Hashtable<String,ArrayList<Character>> expected = obj.retrieveMatrix();
		System.out.println("%%");
		
		files1[0] = new File("C:\\Users\\bluetc-1205\\Desktop\\test.xls");
		files1[1] = new File("C:\\Users\\bluetc-1205\\Desktop\\test1.xls"); 
		obj = new CompareExcel( files1,0,new ProgressBar(), 100 );
		Hashtable<String,ArrayList<Character>> result = obj.retrieveMatrix();
		
		files1[0] = new File("C:\\Users\\bluetc-1205\\Desktop\\test1.xls");
		files1[1] = new File("C:\\Users\\bluetc-1205\\Desktop\\test.xls");
		obj = new CompareExcel( files1,0,new ProgressBar(), 100 );
		Hashtable<String,ArrayList<Character>> result1 = obj.retrieveMatrix();
		System.out.println(expected.toString());
		System.out.println(result.toString());
		System.out.println(result1.toString());
		System.out.println("============================================================");
		Assert.assertEquals(result, result1);
		//fail("Not yet implemented"); // TODO
	}
	

}
