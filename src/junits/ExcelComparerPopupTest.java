/**
 * 
 */
package junits;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import viewer.main.ExcelComparerPopup;

/**
 * @author bluetc-1205
 *
 */
public class ExcelComparerPopupTest {

	/**
	 * Test method for {@link viewer.main.ExcelComparerPopup#ExcelComparerPopup(java.util.ArrayList)}.
	 */
	@Test
	public final void testExcelComparerPopup() {
		ArrayList<ArrayList<String>> data = new ArrayList<ArrayList<String>>();
		
		for( int i = 0 ; i < 3; i++ ){
			data.add(new ArrayList<String>());
			data.get(i).add("f");
			data.get(i).add("2");
			data.get(i).add("5");
			data.get(i).add("d");
			data.get(i).add("r");
		}
		
		ExcelComparerPopup popup = new ExcelComparerPopup(data);
		popup.launchFrame();
		popup.setVisible(true);
		fail("Not yet implemented"); // TODO
	}
	public static void main(String[] args) {
		ArrayList<ArrayList<String>> data = new ArrayList<ArrayList<String>>();
		
		for( int i = 0 ; i < 3; i++ ){
			data.add(new ArrayList<String>());
			data.get(i).add("f");
			data.get(i).add("2");
			data.get(i).add("5");
			data.get(i).add("d");
			data.get(i).add("r");
		}
		
		ExcelComparerPopup popup = new ExcelComparerPopup(data);
		popup.launchFrame();
		popup.setVisible(true);
	}
}
