package junits;

import static org.junit.Assert.*;
import junit.framework.Assert;

import org.junit.Test;

import core.Context;

public class ContextTest {

	@Test
	public final void test() {
		Context.instance().waitingFor(10);
		Assert.assertEquals((float)0,Context.instance().percentageDone());
		Context.instance().workDone();
		Assert.assertEquals((float)10,Context.instance().percentageDone());
		
	}

}
