package junits;

import static org.junit.Assert.*;

import java.io.File;
import java.util.ArrayList;

import org.junit.Assert;
import org.junit.Test;

import viewer.main.ProgressBar;
import core.ApplicationException;
import core.CompareExcel;

import application.Operations;

public class OperationsTest {

	@Test
	public final void testCompareExcel() {
		fail("Not yet implemented"); // TODO
	}

	@Test
	public final void testCompareExcelAll() throws ApplicationException {
		File[] files = new File[3];
		files[0] = new File("C:\\Users\\bluetc-1205\\Desktop\\test.xls");
		files[1] = new File("C:\\Users\\bluetc-1205\\Desktop\\test1.xls");
		files[2] = new File("C:\\Users\\bluetc-1205\\Desktop\\test.xls");
		CompareExcel obj = new CompareExcel( files,0,new ProgressBar(), 100 );
		obj.print();
		ArrayList<ArrayList<String>> expected = new ArrayList<ArrayList<String>>();
		for( int i = 0 ; i < files.length; i++){
			expected.add(new ArrayList<String>());
			expected.get(i).add("1.0");
			expected.get(i).add("2.0");
			expected.get(i).add("4.0");
		}
		Operations.compareExcelAll(CompareExcel.CompareType.PRESENT_REFERENCE,
									0,
									files,
									3,
									new ProgressBar());
	}
	@Test
	public final void testCompareExcelAllOneByOneByRef() throws ApplicationException {
		File[] files = new File[3];
		ArrayList<ArrayList<ArrayList<String>>> res1,res2;
		
		files[0] = new File("C:\\Users\\bluetc-1205\\Desktop\\test.xls");
		files[1] = new File("C:\\Users\\bluetc-1205\\Desktop\\test1.xls");
		files[2] = new File("C:\\Users\\bluetc-1205\\Desktop\\test.xls");
		CompareExcel obj = new CompareExcel( files,0,new ProgressBar(), 100 );
		obj.print();
		ArrayList<ArrayList<String>> expected = new ArrayList<ArrayList<String>>();
		for( int i = 0 ; i < files.length; i++){
			expected.add(new ArrayList<String>());
			expected.get(i).add("1.0");
			expected.get(i).add("2.0");
			expected.get(i).add("4.0");
		}
		res1 = Operations.compareExcelAll(CompareExcel.CompareType.PRESENT_ON_BY_ONE,
									0,
									files,
									3,
									new ProgressBar());
		res2 = Operations.compareExcelAll(CompareExcel.CompareType.PRESENT_REFERENCE,
				0,
				files,
				3,
				new ProgressBar());
		Assert.assertEquals(res1, res2);
	}
	@Test
	public final void testCompareExcelAllOneByOneNotPresent() throws ApplicationException {
		System.out.println("============================================================");
		File[] files = new File[3];
		File[] files1 = new File[2];
		ArrayList<ArrayList<ArrayList<String>>> res1,res2,res3;
		
		files[0] = new File("C:\\Users\\bluetc-1205\\Desktop\\test.xls");
		files[1] = new File("C:\\Users\\bluetc-1205\\Desktop\\test1.xls");
		files[2] = new File("C:\\Users\\bluetc-1205\\Desktop\\test.xls");
		
		files1[0] = new File("C:\\Users\\bluetc-1205\\Desktop\\test.xls");
		files1[1] = new File("C:\\Users\\bluetc-1205\\Desktop\\test1.xls");
		
    	ArrayList<ArrayList<String>> expected = new ArrayList<ArrayList<String>>();
		for( int i = 0 ; i < files.length; i++){
			expected.add(new ArrayList<String>());
			expected.get(i).add("1.0");
			expected.get(i).add("2.0");
			expected.get(i).add("4.0");
		}
		res1 = Operations.compareExcelAll(CompareExcel.CompareType.PRESENT_ON_BY_ONE,
									0,
									files,
									3,
									new ProgressBar());
		res2 = Operations.compareExcelAll(CompareExcel.CompareType.NOT_PRESENT_ON_BY_ONE,
				0,
				files,
				3,
				new ProgressBar());
		res3 = Operations.compareExcelAll(CompareExcel.CompareType.NOT_PRESENT_ON_BY_ONE,
				0,
				files1,
				2,
				new ProgressBar());
		
		System.out.println(res1.toString());
		System.out.println(res2.toString());
		System.out.println(res3.toString());
		System.out.println("============================================================");
		Assert.assertNotSame(res1, res2);
	}

}
