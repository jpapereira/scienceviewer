/**
 * 
 */
package viewer.panels;



import viewer.BaseFrame;
import viewer.main.ComparerPopup;
import viewer.main.ExcelComparerPopup;
import viewer.main.ExcelComparerPopupAll;
import viewer.main.ExcelMatrix;
import viewer.main.MainPanel;
import viewer.main.ProgressBar;
import application.Operations;
import core.ApplicationException;
import core.CompareExcel;
import core.MessageQueue;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.filechooser.FileNameExtensionFilter;

import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map.Entry;

import locale.Language;

/**
 * @author bluetc-1205
 *
 */
public class EXCELselector extends MainPanel{
	
	public final static int imageSize = 20;
	
	private JLabel lblReference = new JLabel();
	private JLabel lblOther = new JLabel();
	
	private JButton plus, minus;
	private JButton bCompAll,bCompRef,bCompMatrix;
	
	private JRadioButton rEqual, rDifferent;
	
	private BaseFrame localFrame;
	private ProgressBar bar;

	private GuiSelector[] selectors;
	private final int max_excel_files = 10;
	private int last_file = 2;
	
	public EXCELselector( BaseFrame frame ){
		localFrame = frame;
		setLayout(null);
		
		bar = new ProgressBar();
		
		selectors = new GuiSelector[max_excel_files];
		
		lblReference.setBounds(20, 30, 150, 14);
		lblReference.setText("Excel file reference");
		add(lblReference);
		
		
		selectors[0] = new GuiSelector( 20, 60, this);
		
		
		lblOther.setBounds((int)selectors[0].getwidth()+60, 30, 70, 14);
		lblOther.setText("Other Files");
		add(lblOther);
		selectors[1] = new GuiSelector( (int)selectors[0].getwidth()+60, 60, this);
		
		
		plus = new JButton();
		ImageIcon img = new javax.swing.ImageIcon(getClass().getResource("/images/Plus.png"));
	    Image newimg = img.getImage().getScaledInstance( imageSize, imageSize,  java.awt.Image.SCALE_SMOOTH ) ;  
	    System.out.println("id:"+this.hashCode());
		plus.setIcon(new ImageIcon(newimg)); 
		plus.setBorder(BorderFactory.createEmptyBorder());
		plus.setContentAreaFilled(false);
		plus.setBorderPainted(false);  
		plus.setFocusPainted(false);  
		plus.setContentAreaFilled(false); 
		plus.setBounds(lblOther.getX()+lblOther.getWidth(), 30, imageSize, imageSize);
		plus.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		plus.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent arg0) {
				if( last_file < max_excel_files ){
					minus.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
					System.out.println(selectors[last_file-1].getX());
					System.out.println(selectors[last_file-1].getY());
					System.out.println("id:"+EXCELselector.this.hashCode());
					selectors[last_file] = new GuiSelector( (int)selectors[1].getX(), 
							selectors[last_file-1].getY() + (int)selectors[last_file-1].getheight(), 
															EXCELselector.this);
					selectors[last_file].setVisible(true);
					EXCELselector.this.repaint();
					last_file++;
				}
				if(last_file == max_excel_files){
					plus.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
				}
			}
		});
		add(plus);
		
		minus = new JButton();
		img = new javax.swing.ImageIcon(getClass().getResource("/images/Minus.png"));
	    newimg = img.getImage().getScaledInstance( imageSize, imageSize,  java.awt.Image.SCALE_SMOOTH ) ;  

	    minus.setIcon(new ImageIcon(newimg)); 
	    minus.setBorder(BorderFactory.createEmptyBorder());
	    minus.setContentAreaFilled(false);
	    minus.setBorderPainted(false);  
	    minus.setFocusPainted(false);  
	    minus.setContentAreaFilled(false); 
	    minus.setBounds(plus.getX()+plus.getWidth()+10, 30, imageSize, imageSize);
	    minus.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
	    minus.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent arg0) {
				if( last_file > 2 ){
					plus.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
					selectors[last_file-1].setVisible(false);
					last_file--;
				}
				if( last_file == 2 ){
					minus.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
				}
			}
		});
		add(minus);
		
		
		// Action buttons
		bCompRef = new JButton("Compare reference");
		bCompRef.setBounds(lblReference.getX(), 
						   (int)(selectors[0].getY()+(selectors[0].getheight()*10)), 
						   150, 
						   22);
		bCompRef.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent arg0) {
				onMouseReleaseButCompRef();
			}
		});
		add(bCompRef);
		// Radio buttons
		rEqual = new JRadioButton("Show only present in reference");
		rEqual.setBounds(selectors[0].getX(), 
						 (int)(selectors[0].getY()+selectors[0].getheight()), 
						   220, 
						   22);
		rEqual.addActionListener( new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				rEqual.setSelected(true);
				rDifferent.setSelected(false);
				
			}
		  });
		rEqual.setSelected(true);
		add(rEqual);
		rDifferent = new JRadioButton("Show not present in reference");
		rDifferent.setBounds(rEqual.getX(), 
							 rEqual.getY()+rEqual.getHeight(), 
							 220, 
							 22);
		rDifferent.addActionListener( new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				rDifferent.setSelected(true);
				rEqual.setSelected(false);
				
			}
		  });
		add(rDifferent);
		// Compare all button
		bCompAll = new JButton("Compare all");
		bCompAll.setBounds(bCompRef.getX()+bCompRef.getWidth()+10, 
						   bCompRef.getY(), 
						   120, 
						   22);
		bCompAll.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent arg0) {
				Entry<Integer,File[]> params = retrieveFiles();
				File [] comparables = params.getValue();
				int i = params.getKey();
				
				if( i < 3 ){
					onMouseReleaseButCompRef();
					
					return;
				}
				System.out.println("B Compare all!!!");
				bar.start();
				try {
					CompareExcel.CompareType type = CompareExcel.CompareType.PRESENT_REFERENCE;
					if( rEqual.isSelected() )
						type = CompareExcel.CompareType.PRESENT_ON_BY_ONE;
					else if( rDifferent.isSelected() )
						type = CompareExcel.CompareType.NOT_PRESENT_ON_BY_ONE;
					ArrayList<ArrayList<ArrayList<String>>> compResult = Operations.compareExcelAll(type, 0, comparables,i, bar);
					
					ExcelComparerPopupAll frame = new ExcelComparerPopupAll(compResult);
					frame.launchFrame();
					frame.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
					
				} catch (ApplicationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		bCompMatrix = new JButton("Matrix Result");
		bCompMatrix.setBounds(bCompAll.getX()+bCompAll.getWidth()+10, 
							  bCompAll.getY(), 
							  120, 
							  22);
		bCompMatrix.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent arg0) {
				Entry<Integer,File[]> params = retrieveFiles();
				matrixButton(params.getValue(),params.getKey());
			}
		});
		add(bCompMatrix);
		System.out.println("l:"+(selectors[0].getY()+selectors[0].getheight())*10);
		add(bCompAll);
	}
	
	private Entry<Integer,File[]> retrieveFiles(){
		File [] comparables = new File[max_excel_files];
		int i;
		for(  i = 0 ; i < selectors.length; i++){
			try{
				System.out.println("Pico: "+i);
				comparables[i] = selectors[i].getExcel();
			}catch( IOException e){
				break;
			}catch( ArrayIndexOutOfBoundsException e){
				break;
			}catch( java.lang.NullPointerException e ){
				break;
			}
		}
		return new AbstractMap.SimpleEntry<Integer,File[]>(i,comparables);
	}
	
	private void onMouseReleaseButCompRef(){
		System.out.println("Simplecompare");
		
		Entry<Integer,File[]> params = retrieveFiles();
		File [] comparables = params.getValue();
		int i = params.getKey();
		if( i < 2 )
			return;
		bar.start();
		
		try {
			CompareExcel.CompareType type = CompareExcel.CompareType.PRESENT_REFERENCE;
			if( rEqual.isSelected() )
				type = CompareExcel.CompareType.PRESENT_REFERENCE;
			else if( rDifferent.isSelected() )
				type = CompareExcel.CompareType.NOT_PRESENT_REFERENCE;
			ArrayList<ArrayList<String>> compResult = Operations.compareExcel(type, 0, comparables,i, bar);
			
			ExcelComparerPopup frame = new ExcelComparerPopup(compResult);
			frame.launchFrame();
			frame.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
			
		} catch (ApplicationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	private void matrixButton( File[] comparables, int numberComparables){
		try {
			
			Hashtable<String,ArrayList<Character>> compResult = Operations.retrieveMatrix(comparables, numberComparables, bar);
			
			ExcelMatrix frame = new ExcelMatrix(compResult);
			frame.launchFrame();
			frame.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
			
		} catch (ApplicationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}

class GuiSelector extends Component{
	/**
	 * 
	 */
	private static final long serialVersionUID = 8772086078858870778L;
	private final String defaultText = new String("Empty");
	private JLabel lblCmpExcel = new JLabel("");
	private JFileChooser fileChooser;
	private JButton bBrowse, bReset;
	private EXCELselector screen;
	private File currentFile;
	private int x_pos;
	private int y_pos;
	private float x_size;
	private float y_size;
	public GuiSelector( int x, int y, EXCELselector main ){
		screen = main;
		x_pos = x;
		y_pos = y;
		lblCmpExcel.setText(defaultText);
		lblCmpExcel.setEnabled(false);
		lblCmpExcel.setBounds(x_pos, y_pos, 150, 15);
		fileChooser = new JFileChooser();
		fileChooser.setMultiSelectionEnabled(false);
		FileNameExtensionFilter ff = new FileNameExtensionFilter(Language.lang().getTranslation("filechooser.excelfiles"), "xls", "xlsm");
		fileChooser.addChoosableFileFilter(ff);
		fileChooser.setFileFilter(ff);
		bBrowse = new JButton("Choose File");
		bBrowse.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent arg0) {
				int returnVal = fileChooser.showOpenDialog(GuiSelector.this);
				if( returnVal == JFileChooser.APPROVE_OPTION){
					currentFile = fileChooser.getSelectedFile();
					lblCmpExcel.setText(currentFile.getAbsolutePath());
					bReset.setVisible(true);
				}
			}
		});
		bBrowse.setBounds(x_pos+lblCmpExcel.getWidth() +10, y_pos, 110, 22);
		bReset = new JButton();
		ImageIcon img = new ImageIcon(getClass().getResource("/images/red-cross-mark-md.png"));
	    Image newimg = img.getImage().getScaledInstance( EXCELselector.imageSize, EXCELselector.imageSize,  java.awt.Image.SCALE_SMOOTH ) ;  

	    bReset.setIcon(new ImageIcon(newimg)); 
	    bReset.setBorder(BorderFactory.createEmptyBorder());
	    bReset.setContentAreaFilled(false);
	    bReset.setBorderPainted(false);  
	    bReset.setFocusPainted(false);  
	    bReset.setContentAreaFilled(false); 
	    bReset.setBounds(bBrowse.getX()+bBrowse.getWidth()+10, bBrowse.getY(), EXCELselector.imageSize, EXCELselector.imageSize);
	    bReset.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent arg0) {
				lblCmpExcel.setText(defaultText);
				bReset.setVisible(false);
			}
		});
	    bReset.setVisible(false);
		
		screen.add(lblCmpExcel);
		screen.add(bBrowse);
		screen.add(bReset);
		
		System.out.println("x: "+bBrowse.getX());
		System.out.println("y: "+bBrowse.getY());
		x_size = bBrowse.getX() + bBrowse.getWidth();
		y_size = Math.max(lblCmpExcel.getHeight(),bBrowse.getHeight())+10;
	}
	public float getwidth(){
		return x_size;
	}
	public float getheight(){
		return y_size;
	}
	public File retriveFile(){
		return currentFile;
	}
	public int getX(){
		return x_pos;
	}
	public int getY(){
		return y_pos;
	}
	public void setVisible(boolean b){
		lblCmpExcel.setVisible(b);
		bBrowse.setVisible(b);
	}
	public File getExcel() throws IOException{
		if( lblCmpExcel.getText() == defaultText){
			throw new IOException("No File");
		}
		return currentFile;
	}
}

