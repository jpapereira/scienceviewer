/**
 * 
 */
package viewer.panels;

import java.awt.Button;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.List;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;

import viewer.BaseFrame;
import viewer.main.ComparerPopup;
import viewer.main.MainPanel;
import application.Operations;
import core.ApplicationException;
import core.MessageQueue;
import javax.swing.JButton;
import java.awt.Color;

/**
 * @author bluetc-1205
 *
 */
public class DNAoperations extends MainPanel{
	private JTextArea startDNA = new JTextArea();
	private JLabel lblStartADN = new JLabel();
	private JTextArea toCheckDNA = new JTextArea();
	
	private BaseFrame localFrame;
	
	private JScrollPane scrollStart;
	private JScrollPane scrollCheck;
	
	public DNAoperations( BaseFrame frame ){
		localFrame = frame;
		setLayout(null);
		
		startDNA.setLineWrap(true);
		startDNA.setWrapStyleWord(true);
		startDNA.setBounds(10, 55, 487, 369);
		scrollStart = new JScrollPane(startDNA);
		scrollStart.setBounds(10, 55, 504, 372);
		scrollStart.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		add(scrollStart);
		toCheckDNA.setEditable(false);
		toCheckDNA.setForeground(Color.WHITE);
		toCheckDNA.setBackground(Color.WHITE);
		toCheckDNA.setLineWrap(true);
		toCheckDNA.setWrapStyleWord(true);
		toCheckDNA.setBounds(514, 55, 487, 369);
		toCheckDNA.setEnabled(false);
		scrollCheck = new JScrollPane(toCheckDNA);
		scrollCheck.setBounds(520, 55, 504, 372);
		scrollCheck.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		add(scrollCheck);
		
		Button btAntiParallel = new Button("Antiparallel");
		btAntiParallel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent arg0) {
				
				try {
					toCheckDNA.setText(Operations.retrieveAntiparallel(startDNA.getText()));
				} catch (ApplicationException e) {
					JOptionPane.showMessageDialog((JFrame)localFrame,
						    e.getMessage(),
						    "Application error",
						    JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		btAntiParallel.setBounds(200, 444, 97, 22);
		add(btAntiParallel);
		Button btComplement = new Button("Complement");
		btComplement.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent arg0) {
				try {
					toCheckDNA.setText(Operations.retrieveComplement(startDNA.getText()));
				} catch (ApplicationException e) {
					JOptionPane.showMessageDialog((JFrame)localFrame,
						    e.getMessage(),
						    "Application error",
						    JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		btComplement.setBounds(300, 444, 97, 22);
		add(btComplement);
		Button btInvert = new Button("Invert");
		btInvert.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent arg0) {
				try {
					toCheckDNA.setText(Operations.retrieveInvert(startDNA.getText()));
				} catch (ApplicationException e) {
					JOptionPane.showMessageDialog((JFrame)localFrame,
						    e.getMessage(),
						    "Application error",
						    JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		btInvert.setBounds(400, 444, 97, 22);
		add(btInvert);
		Button btGen = new Button("Generate");
		btGen.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent arg0) {
				try {
					toCheckDNA.setText(Operations.retrieveInvert(startDNA.getText()));
				} catch (ApplicationException e) {
					JOptionPane.showMessageDialog((JFrame)localFrame,
						    e.getMessage(),
						    "Application error",
						    JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		btGen.setBounds(500, 444, 97, 22);
		add(btGen);
		
		lblStartADN.setText("Start DNA");
		lblStartADN.setBounds(20, 30, 96, 14);
		add(lblStartADN);
		
		JLabel lblDnaToCheck = new JLabel("Operation result");
		lblDnaToCheck.setBounds(520, 30, 112, 14);
		add(lblDnaToCheck);
		
		
		
	}
	public void execute_cut(){
		
	}
	@Override
	public  void execute_paste(){
		System.out.println("DNAaminoacid");
		startDNA.setText(getClipboardContents());
	}
	public void execute_copy(){
		this.copyToCLipboard(toCheckDNA.getText());
	}
	
}
