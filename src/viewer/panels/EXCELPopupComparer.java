/**
 * 
 */
package viewer.panels;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;


import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableColumnModel;
import javax.swing.table.TableColumn;

/**
 * @author bluetc-1205
 *
 */
public class EXCELPopupComparer extends JPanel { 
		
		/**
	 * 
	 */
	private static final long serialVersionUID = 2185858159802927283L;
		private JFrame localFrame;
		private JTable table;
		private final int letterPerRow = 2;
		
		public EXCELPopupComparer( JFrame frame , int width, int height, ArrayList<String> columRes ){
			localFrame = frame;
			int tableWitdth = width-20;
			setLayout(null);
			
			
			String[] columnNames = {"1","2",
									"1","2",
									"1","2"};
			
			/*System.out.println("columRes[0]="+columRes.get(0));
			System.out.println("columRes[1]="+columRes.get(1));*/
			String[] colHeaders = new String[letterPerRow+1];
			for( int i = 0 ; i < letterPerRow+1 ; i++ ){
				colHeaders[i] = new String(""+i);
				
			}
			int numberRows = columRes.size();
			Object[][] tableValues = new Object[numberRows][letterPerRow+1];
			//System.out.println("Tablesize["+numberRows+"]["+(letterPerRow+1)+"]");

			for( int i = 0 ; i < columRes.size() ; i++ ){
				if( null == columRes.get(i))
					break;
				//System.out.println("Row["+i+"] value="+columRes.get(i));
				tableValues[i][0] = columRes.get(i);
			}
			
			
			
			table = new JTable(tableValues,colHeaders);
			table.setFillsViewportHeight(true);
			table.setBorder(null);
			table.setTableHeader(null);
			table.setBounds(10, 11,tableWitdth-5, height-20);
			table.setShowGrid(false);
			
			table.setIntercellSpacing(new Dimension(0, 0));
			
			
			
			//table.setDefaultRenderer(Object.class, new MyTableCellRenderer());
			//table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
			table.getColumnModel().getColumn(0).setPreferredWidth(300);
			
			DefaultTableColumnModel colModel = (DefaultTableColumnModel) table.getColumnModel();
			
			for (int i = 0; i < table.getColumnCount(); i++) {		      
			      TableColumn col = colModel.getColumn(i);
			      col.setPreferredWidth(300);
			    }
			
			JScrollPane scroll = new JScrollPane(table);
			scroll.setBounds(10, 11, tableWitdth -5, height-20);
			scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
			add(scroll);
			

		}
	}


