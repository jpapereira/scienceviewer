/**
 * 
 */
package viewer.panels;

import java.awt.Button;
import java.awt.List;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;

import viewer.BaseFrame;
import viewer.main.MainPanel;

import application.Operations;
import core.ApplicationException;
import core.MessageQueue;

/**
 * @author bluetc-1205
 *
 */
public class DNAaminoacidretrieval extends MainPanel {
	private JTextArea startDNA = new JTextArea();
	private JLabel lblStartADN = new JLabel("DNA Sequence FASTa");
	private List errorList = new List();
	
	private BaseFrame localFrame;
	private JTextArea startAminoacid;
	private JScrollPane scroll, scrollCheck;
	public DNAaminoacidretrieval(BaseFrame frame ){
		localFrame = frame;
		setLayout(null);
		
		
		lblStartADN.setBounds(10, 30, 191, 14);
		add(lblStartADN);
		//add(startDNA);
		scroll = new JScrollPane(startDNA);
		scroll.setBounds(10, 55, 504, 372);
		scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		add(scroll);

		Button button = new Button("Retrieve Amino Acids");
		button.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				System.out.println(startDNA.getText());
				try{
					if( !Operations.checkSequenceFASTa(startDNA.getText(), false)){
						errorList.removeAll();
						errorList.setVisible(true);
						while( MessageQueue.getQueue().getNumberMessages() > 0 ){
							System.out.println("Doing stuff");
							errorList.add(MessageQueue.getQueue().getMessage());
						}
					}else{
						errorList.setVisible(false);
						startAminoacid.setText( Operations.getAminoacidFASTa(startDNA.getText()));
					}
				}catch(ApplicationException e){
					JOptionPane.showMessageDialog((JFrame)localFrame,
						    e.getMessage(),
						    "Application error",
						    JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		
		startAminoacid = new JTextArea();
		startAminoacid.setBounds(521, 58, 320, 300);
		startAminoacid.setEditable(false);
		startAminoacid.setLineWrap(true);
		
		startAminoacid.setLineWrap(true);
		startAminoacid.setWrapStyleWord(true);
		startAminoacid.setBounds(514, 55, 487, 369);
		scrollCheck = new JScrollPane(startAminoacid);
		scrollCheck.setBounds(520, 55, 504, 372);
		scrollCheck.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		add(scrollCheck);
		//add(startAminoacid);
		//startAminoacid.setColumns(10);
		button.setBounds(520, 440, 137, 22);
		add(button);
		errorList.setVisible(false);
		
		
		errorList.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
			}
		});
		errorList.setBounds(520, 119, 320, 308);
		add(errorList);
		
		JLabel lblAminoAcid = new JLabel("Amino Acid");
		lblAminoAcid.setBounds(521, 30, 87, 14);
		add(lblAminoAcid);
		
		
		startDNA.setLineWrap(true);
		startDNA.setWrapStyleWord(true);
		
		startDNA.setBounds(10, 55, 487, 369);
		

	}
	
	public void execute_cut(){
		
	}
	@Override
	public  void execute_paste(){
		System.out.println("DNAaminoacid");
		startDNA.setText(getClipboardContents());
	}
	public void execute_copy(){
		this.copyToCLipboard(startAminoacid.getText());
	}
	
}
