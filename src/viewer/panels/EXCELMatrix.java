package viewer.panels;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Hashtable;
import java.util.Map.Entry;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;


import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableColumnModel;
import javax.swing.table.TableColumn;

import locale.Language;

/**
 * @author bluetc-1205
 *
 */
public class EXCELMatrix extends JPanel { 
		
	/**
	 * 
	 */
	private static final long serialVersionUID = 2185858159802927283L;
		private JFrame localFrame;
		private JTable table;
		private final int letterPerRow = 10;
		
		public EXCELMatrix( JFrame frame , int width, int height, Hashtable<String,ArrayList<Character>> matrix ){
			localFrame = frame;
			int tableWitdth = width-10;
			setLayout(null);
			
			
			String[] columnNames = {"1","2",
									"1","2",
									"1","2"};
			
			/*System.out.println("columRes[0]="+columRes.get(0));
			System.out.println("columRes[1]="+columRes.get(1));*/
			int numCols = 0;
			for( Entry<String,ArrayList<Character>> a: matrix.entrySet() ){
				System.out.println("Number of columns: "+a.getValue().size());
				numCols = a.getValue().size();
				break;
			}
			String[] colHeaders = new String[letterPerRow+1];
			ArrayList<String> headerNames = new ArrayList<String>();
			headerNames.add("");
			for( int i = 0 ; i < numCols ; i++ ){
				System.out.println("i="+i);
				headerNames.add( Language.lang().getTranslation("viewer.panels.excelmatrix.columnhead")+ " " + (i));
			}
			System.out.println("Number of headers: "+headerNames.size());
			
			
			ArrayList<Object[]> asd = new ArrayList<Object[]>();
			for( Entry<String,ArrayList<Character>> a: matrix.entrySet() ){
				asd.add( a.getValue().toArray());
			}
			
			MyTableModel mytable = new MyTableModel( matrix,headerNames );
			
			
			
			//table = new JTable((Object[][]) asd.toArray(),headerNames.toArray());
			table = new JTable(mytable);
			table.setFillsViewportHeight(true);
			table.setBorder(null);
			table.setTableHeader(null);
			table.setBounds(10, 11,tableWitdth-5, height-20);
			table.setShowGrid(false);
			
			table.setIntercellSpacing(new Dimension(0, 0));
			
			
			
			//table.setDefaultRenderer(Object.class, new MyTableCellRenderer());
			//table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
			table.getColumnModel().getColumn(0).setPreferredWidth(200);
			
			DefaultTableColumnModel colModel = (DefaultTableColumnModel) table.getColumnModel();
			
			for (int i = 0; i < table.getColumnCount(); i++) {		      
			      TableColumn col = colModel.getColumn(i);
			      col.setPreferredWidth(200);
			    }
			
			JScrollPane scroll = new JScrollPane(table);
			scroll.setBounds(10, 11, tableWitdth -5, height-20);
			scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
			add(scroll);
			

		}
	}


class MyTableModel extends AbstractTableModel {
	private Hashtable<String,ArrayList<Character>> table;
	private ArrayList<String> keys;
	private ArrayList<String> header;
	private int numColumns;
	public MyTableModel( Hashtable<String,ArrayList<Character>> tbl, ArrayList<String> headers ){
		table = tbl;
		for( Entry<String,ArrayList<Character>> a: tbl.entrySet() ){
			numColumns = a.getValue().size()+1;
			break;
		}
		keys = new ArrayList<String>(table.keySet());
		Collections.sort(keys);
		header = headers;
	}
	@Override
	public int getColumnCount() {
		// TODO Auto-generated method stub
		
		return numColumns;
	}

	@Override
	public int getRowCount() {
		// TODO Auto-generated method stub
		return table.size() +1;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		// TODO Auto-generated method stub
		if( rowIndex == 0 )
			return header.get(columnIndex);
		System.out.println("row:"+rowIndex+"::column:"+columnIndex);
		if( columnIndex == 0 )
			return keys.get(Math.max(rowIndex-1,0));
		
		return table.get(keys.get(rowIndex-1)).get(columnIndex-1);
	}
	
}