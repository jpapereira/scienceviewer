/**
 * 
 */
package viewer.panels;

import java.awt.Button;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.List;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;

import application.Operations;
import core.ApplicationException;
import core.MessageQueue;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableColumnModel;
import javax.swing.table.TableColumn;
import javax.swing.JList;

/**
 * @author bluetc-1205
 *
 */
public class DNApopupComparer extends JPanel {
		
		private JFrame localFrame;
		private JTable table;
		private final int letterPerRow = 55;
		
		public DNApopupComparer( JFrame frame , int width, int height, java.util.List<String> data, java.util.List<String> problems ){
			localFrame = frame;
			int tableWitdth = (problems.size()>0?width -250:width-20);
			setLayout(null);
			
			/**Object[][] data = {
				    {"A", "T", "G", "A", "T", "G","A", "T", "G", "A", "T", "G","A", "T", "G", "A", "T", "G", "T", "G"},
				    {"|", "|", "|", "|", "|", "|","|", "|", "|", "|", "|", "|","|", "|", "|", "|", "|", "|", "|", "|"},
				    {"A", "T", "G", "A", "T", "G","A", "T", "G", "A", "T", "G","A", "T", "G", "A", "T", "G", "T", "G"}
				};*/
			String[] columnNames = {"1","2","3","4","5","6","7","8","9","0",
									"1","2","3","4","5","6","7","8","9","0",
									"1","2","3","4","5","6","7","8","9","0"};
			
			System.out.println("data[0]="+data.get(0));
			System.out.println("data[1]="+data.get(1));
			String[] colHeaders = new String[letterPerRow+1];
			for( int i = 0 ; i < letterPerRow+1 ; i++ ){
				colHeaders[i] = new String(""+i);
				
			}
			int numberRows = (int)(data.get(0).length()/letterPerRow+1) * 3;
			Object[][] tableValues = new Object[numberRows][letterPerRow+1];
			System.out.println("Tablesize["+numberRows+"]["+(letterPerRow+1)+"]");
			int actualRow = 0 ;
			int actualCol = 0;
			int previousCol = 0;
			int previousRow = -1;
			for( int i = 0 ; i < data.get(0).length() ; i++){
				System.out.println("Position["+i+"] row["+actualRow+"] col["+actualCol+"]");
				previousCol = actualCol;
				tableValues[actualRow][actualCol+1] = data.get(0).charAt(i);
				tableValues[actualRow+1][actualCol+1] = data.get(1).charAt(i);
				tableValues[actualRow+2][actualCol+1] = "";
				actualCol = (++actualCol%letterPerRow);
				actualRow = (actualCol<previousCol?(actualRow+3):actualRow);
				if( previousRow != actualRow ){
					tableValues[actualRow][0] = "Base";
					tableValues[actualRow+1][0] = "Check";
					previousRow = actualRow;
				}
			}
			
			table = new JTable(tableValues,colHeaders);
			table.setFillsViewportHeight(true);
			table.setBorder(null);
			table.setTableHeader(null);
			table.setBounds(10, 11,tableWitdth-5, height-20);
			table.setShowGrid(false);
			
			table.setIntercellSpacing(new Dimension(0, 0));
			
			
			
			table.setDefaultRenderer(Object.class, new MyTableCellRenderer());
			table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
			for( int i = 1; i < letterPerRow+1; i++ ){
				table.getColumnModel().getColumn(i).setPreferredWidth(10);
			}
			table.getColumnModel().getColumn(0).setPreferredWidth(100);
			
			DefaultTableColumnModel colModel = (DefaultTableColumnModel) table.getColumnModel();
			
			for (int i = 0; i < table.getColumnCount(); i++) {		      
			      TableColumn col = colModel.getColumn(i);
			      col.setPreferredWidth(10);
			    }
			
			JScrollPane scroll = new JScrollPane(table);
			scroll.setBounds(10, 11, tableWitdth -5, height-20);
			scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
			add(scroll);
			
			if( problems.size() > 0 ){
				JList list = new JList();
				list.setBounds(width - 240 , 11 ,200, 300);
				
				DefaultListModel listModel = new DefaultListModel();
				for( int i = 0 ; i < problems.size() ; i++ ){
					listModel.addElement(problems.get(i));
				}
				list.setModel(listModel);
				add(list);
			}

		}
	}

class MyTableCellRenderer extends DefaultTableCellRenderer { 
	public Component getTableCellRendererComponent(JTable table, Object value,boolean isSelected,boolean hasFocus,int row,int column) {
		System.out.println("getTableCellRendererComponent("+value+","+row+","+column+")");
		Font f = table.getFont();
		
		//DefaultTableCellRenderer  a = new MyTableCellRenderer();
		//a.setHorizontalAlignment(JLabel.CENTER);
		//table.getColumnModel().getColumn(column).setCellRenderer( a );
		/**if( 0 == value.toString().compareTo("|") )
			setFont( new Font( f.getFontName(), Font.PLAIN, 9 ));
		else
			setFont( new Font( f.getFontName(), Font.PLAIN, 9 ));*/
		setValue(value);
		if( row!= 0 && 0 == ((row+1)%3)){
			return this;
		}
		if( 0 == (column % 2 ) )
			this.setBackground(Color.LIGHT_GRAY);
		else
			this.setBackground(Color.WHITE);
		if( value != null && value.toString().equals(new String("_")) ){
				setValue("*");
				this.setBackground(Color.RED);
		}
		if( column != 0 ){
			table.getColumnModel().getColumn(column).setPreferredWidth(5);
			table.getColumnModel().getColumn(column).setWidth(5);
		}else{
			table.getColumnModel().getColumn(column).setPreferredWidth(50);
			table.getColumnModel().getColumn(column).setWidth(50);
		}
		System.out.println("width:"+table.getColumnModel().getColumn(column).getWidth()); 
		//Stable.getColumnModel().getColumn(column).setPreferredWidth(10);
		return this; // Returns the renderer. 
	} 
} 
