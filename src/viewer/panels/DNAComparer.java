/**
 * 
 */
package viewer.panels;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.List;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;

import viewer.BaseFrame;
import viewer.main.ComparerPopup;
import viewer.main.MainPanel;
import application.Operations;
import core.ApplicationException;
import core.MessageQueue;
import javax.swing.JButton;

/**
 * @author bluetc-1205
 *
 */
public class DNAComparer extends MainPanel{
	private JTextArea startDNA = new JTextArea();
	private JLabel lblStartADN = new JLabel("Start DNA");
	private JTextArea toCheckDNA = new JTextArea();
	private List errorList = new List();
	
	private BaseFrame localFrame;
	
	private JScrollPane scrollStart;
	private JScrollPane scrollCheck;
	
	public DNAComparer( BaseFrame frame ){
		localFrame = frame;
		setLayout(null);
		
		startDNA.setLineWrap(true);
		startDNA.setWrapStyleWord(true);
		startDNA.setBounds(10, 55, 487, 369);
		scrollStart = new JScrollPane(startDNA);
		scrollStart.setBounds(10, 55, 504, 372);
		scrollStart.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		add(scrollStart);
		toCheckDNA.setLineWrap(true);
		toCheckDNA.setWrapStyleWord(true);
		toCheckDNA.setBounds(514, 55, 487, 369);
		scrollCheck = new JScrollPane(toCheckDNA);
		scrollCheck.setBounds(520, 55, 504, 372);
		scrollCheck.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		add(scrollCheck);
		
		Button button = new Button("Check difference");
		button.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent arg0) {
				if( startDNA.getText().length() == 0 || toCheckDNA.getText().length() == 0 ){
					JOptionPane.showMessageDialog((JFrame)localFrame,
						    "The previous fields need to be filled to preform this action!",
						    "Warning",
						    JOptionPane.WARNING_MESSAGE);
					return;
				}
				
				java.util.List<String> compResult = Operations.retrieveComparedDNASeqFASTa(startDNA.getText(), toCheckDNA.getText());
				java.util.List<java.util.List<String>> res = new ArrayList();
				int it = 0;
				System.out.println("size of the array is:"+compResult.size());
				for( int i = 0 ; i < compResult.size() ; i+=2 ){
					if( compResult.get(i).length() > 0 && compResult.get(i+1).length() > 0 ){
						res.add(new ArrayList());
						res.get(it).addAll(Operations.compareTwoStrings(compResult.get(i), compResult.get(i+1)));
						//System.out.println("id["+it+"]:"+Arrays.deepToString(res.get(it)));
						it++;
					}
				}
				
				System.out.println("Pirulas1:"+compResult.toString());
				//java.util.List<String> a = Operations.compareTwoStrings(compResult[0], compResult[1]);
				ComparerPopup frame = new ComparerPopup(compResult,res);
				frame.launchFrame();
				frame.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
				
			}
			/*@Override
			public void mouseReleased(MouseEvent arg0) {
				mouseClicked(arg0);
			}*/
		});
		button.setBounds(460, 449, 97, 22);
		add(button);
		errorList.setVisible(false);
		
		
		errorList.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
			}
		});
		errorList.setBounds(10, 449, 208, 150);
		add(errorList);
		
		
		lblStartADN.setBounds(20, 30, 96, 14);
		add(lblStartADN);
		
		//startDNA.setBounds(108, 24, 312, 20);
		//add(startDNA);
		
		
		//toCheckDNA.setBounds(108, 85, 312, 20);
		//add(toCheckDNA);
		
		JLabel lblDnaToCheck = new JLabel("DNA to check");
		lblDnaToCheck.setBounds(520, 30, 112, 14);
		add(lblDnaToCheck);
		
		

	}
	public void execute_cut(){
		
	}
	@Override
	public  void execute_paste(){
		System.out.println("DNAaminoacid");
		startDNA.setText(getClipboardContents());
	}
	public void execute_copy(){
		this.copyToCLipboard(toCheckDNA.getText());
	}
	
}
