/**
 * 
 */
package viewer.main;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import locale.Language;

/**
 * @author bluetc-1205
 *
 */
public class AboutDialog extends JDialog {
	
	public AboutDialog( JFrame parent ){
		super( parent , Language.lang().getTranslation("aboutDialog.title"), true);
		
		Box b = Box.createVerticalBox();
	    b.add(Box.createGlue());
	    b.add(new JLabel(Language.lang().getTranslation("aboutDialog.creator")));
	    b.add(new JLabel(Language.lang().getTranslation("aboutDialog.contact")));
	    b.add(Box.createGlue());
	    getContentPane().add(b, "Center");

	    JPanel p2 = new JPanel();
	    JButton ok = new JButton(Language.lang().getTranslation("main.okButton"));
	    p2.add(ok);
	    getContentPane().add(p2, "South");

	    ok.addActionListener(new ActionListener() {
	      public void actionPerformed(ActionEvent evt) {
	        setVisible(false);
	      }
	    });

	    setSize(250, 150);
	}
}
