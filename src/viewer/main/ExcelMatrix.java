package viewer.main;

import viewer.BaseFrame;
import viewer.InfiniteProgressPanel;
import viewer.main.ExcelComparerPopup.ListenCloseWdw;
import viewer.main.ExcelComparerPopup.ListenMenuQuit;
import viewer.panels.AnimatedPanel;
import viewer.panels.EXCELMatrix;
import viewer.panels.EXCELPopupComparer;


import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Hashtable;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JSplitPane;
import javax.swing.filechooser.FileNameExtensionFilter;

import core.Context;
import core.DictyDatabase;
import core.SaveToExcel;
import core.ViewerThread;

import java.awt.FlowLayout;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.nio.file.StandardOpenOption;

import locale.Language;

public class ExcelMatrix extends BaseFrame {
	private JPanel topPanel;
	private final int maxWidth = 1200;
	private final int maxHeight = 700;
	private Hashtable<String,String> dictyDbResults;
	private Hashtable<String,ArrayList<Character>> displayMatrix;
	private BackgroundWork bgwork=null;
	private ProgressBarUpdater pbworker=null;
	JButton b_blastDb,b_dictyDb, b_saveExcel;
	private JProgressBar progressBar;
	public ExcelMatrix( Hashtable<String,ArrayList<Character>> data ){
		
		dictyDbResults = new  Hashtable<String,String>();
		
		pbworker = new ProgressBarUpdater();
		
		displayMatrix = data;
		setTitle("Excel Compared");
		setPreferredSize(new Dimension(maxWidth,maxHeight));
		getContentPane().setLayout(null);
		
		topPanel = new EXCELMatrix( this ,maxWidth-300,maxHeight-50, data );
		topPanel.setBounds(0, 0, maxWidth-100,maxHeight-50);
		
		
		b_dictyDb = new JButton(Language.lang().getTranslation("viewer.main.excelmatrix.b_dictydb"));
		
		b_dictyDb.setBounds(maxWidth-299, 
				   topPanel.getY()+10, 
				   200, 
				   22);
		
		add(b_dictyDb);
		b_dictyDb.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent arg0) {
				startRetrievingStructures();
			}
		});
		b_blastDb = new JButton(Language.lang().getTranslation("viewer.main.excelmatrix.b_blastdb"));
		
		b_blastDb.setBounds(b_dictyDb.getX(), 
					b_dictyDb.getY()+32, 
				   200, 
				   22);
		b_blastDb.setEnabled(false);
		
		b_saveExcel = new JButton(Language.lang().getTranslation("viewer.main.excelmatrix.b_saveexcel"));
		
		b_saveExcel.setBounds(b_blastDb.getX(), 
					b_blastDb.getY()+32, 
				   200, 
				   22);
		
		b_saveExcel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent arg0) {
				JFileChooser fileChooser = new JFileChooser();
				FileNameExtensionFilter ff = new FileNameExtensionFilter(Language.lang().getTranslation("filechooser.excelfiles"), "xls", "xlsm");
				fileChooser.addChoosableFileFilter(ff);
				fileChooser.setFileFilter(ff);
				int returnVal = fileChooser.showSaveDialog(null);
				
				
				if( returnVal == JFileChooser.APPROVE_OPTION){
					File f =fileChooser.getSelectedFile();
					if( !fileChooser.getSelectedFile().getPath().toLowerCase().endsWith("xls") )
						f = new File( fileChooser.getSelectedFile().getPath() + ".xls");
					
					try {
						SaveToExcel.save(f, displayMatrix, dictyDbResults);
					} catch (IOException e) {
						JOptionPane.showMessageDialog(ExcelMatrix.this,
								Language.lang().getTranslation("errordialog.file.save"),
								Language.lang().getTranslation("errordialog.file.title"),
							    JOptionPane.ERROR_MESSAGE);
					}
					
						
				}
			}
		});
		add(b_saveExcel);
		//add(b_blastDb);
		add(topPanel);
		b_dictyDb.setVisible(true);
		this.paintAll(null);
		
		progressBar = new JProgressBar(0, 100);
        progressBar.setValue(0);
        progressBar.setStringPainted(true);
        progressBar.setBounds(b_dictyDb.getBounds());
        progressBar.setVisible( false );
        add(progressBar);
		
		addWindowListener(new ListenCloseWdw());
		mnuItemQuit.addActionListener(new ListenMenuQuit());


	}
	public class ListenMenuQuit implements ActionListener{
		  public void actionPerformed(ActionEvent e){
			  if( bgwork != null )
		        	bgwork.stopThread();
		  }
	}
	public class ListenCloseWdw extends WindowAdapter{
		  public void windowClosing(WindowEvent e){
		        if( bgwork != null )
		        	bgwork.stopThread();
		  }
	}
	
	private void startRetrievingStructures(  ) {
		pbworker.start();
		progressBar.setVisible( true );
		progressBar.repaint();
		bgwork = new BackgroundWork( this, displayMatrix,dictyDbResults );
		bgwork.start();
		b_dictyDb.setEnabled(false);
		b_saveExcel.setEnabled(false);
		
	}
	
	class BackgroundWork extends ViewerThread{
		private BaseFrame frame;
		private Hashtable<String,ArrayList<Character>> data;
		private Hashtable<String,String> dictyDbResults;
		private boolean shouldBeAlive = true;
		DictyDatabase db;
		public BackgroundWork(BaseFrame frame, Hashtable<String,ArrayList<Character>> data, Hashtable<String,String> dictyDbResults){
			this.frame = frame;
			this.data = data;
			this.dictyDbResults = dictyDbResults;
		}
		public void work(){
			try {
				db = new DictyDatabase(Context.instance().getProxy());
			
				Context.instance().waitingFor(data.keySet().size());
				for( String str : data.keySet() )
					db.search(DictyDatabase.SequenceType.PROTEIN, str );
				for( String str : data.keySet()){
					dictyDbResults.put(str, db.retrieveSearch(str));
					Context.instance().workDone();
					System.out.println("Percentage: "+Context.instance().percentageDone());
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			b_blastDb.setEnabled(true);
			progressBar.setVisible( false );
			b_saveExcel.setEnabled(true);
			stopThread();
		}
		public void stopThread(){
			super.stopThread();
			if( null != db )
				db.stopWorking();
		}
	}
	class ProgressBarUpdater extends ViewerThread{
		
		public ProgressBarUpdater(){
		}
		public void work(){
			progressBar.setValue((int)Context.instance().percentageDone());
			try {
				Thread.sleep(200);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
