package viewer.main;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JPanel;
import javax.swing.JProgressBar;

import locale.Language;

import viewer.BaseFrame;

public class ProgressBar extends BaseFrame{
	private JProgressBar progressBar;
	private JPanel panel;
	private final int maxWidth = 200;
	private final int maxHeight = 100;
	public ProgressBar(){
		super();
		setTitle(Language.lang().getTranslation("progressBar.title.unknown"));
		setPreferredSize(new Dimension(maxWidth,maxHeight));
		
		progressBar = new JProgressBar(0, 100);
        progressBar.setValue(0);
        progressBar.setStringPainted(true);
        panel = new JPanel();
        panel.add(progressBar);
        add(panel);
        
        
        //progressBar.setOpaque(true); //content panes must be opaque
        setContentPane(progressBar);
 

	}
	public void start(){
		progressBar.setValue(0);
		//Display the window.
        pack();
        setVisible(true);
	}
	
	public void changeStatus( int actualStatus ){
		progressBar.setValue(actualStatus);
		System.out.println("My value is:"+actualStatus);
		this.repaint();
		if( actualStatus >= 100 ){
			setVisible(false);
			this.dispose();
		}
	}
	public void increment( int value ){
		changeStatus(progressBar.getValue() + value);
	}
	
	public class ListenMenuQuit implements ActionListener{
		  public void actionPerformed(ActionEvent e){        
		  }
	}
	public class ListenCloseWdw extends WindowAdapter{
		  public void windowClosing(WindowEvent e){
		  }
	}
}
