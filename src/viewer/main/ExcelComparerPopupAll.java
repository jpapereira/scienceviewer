package viewer.main;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import viewer.BaseFrame;
import viewer.panels.DNAComparer;
import viewer.panels.DNAaminoacidretrieval;
import viewer.panels.DNApopupComparer;
import viewer.panels.EXCELPopupComparer;

public class ExcelComparerPopupAll extends BaseFrame {
	
	private final int maxWidth = 1000;
	private final int maxHeight = 700;
	
	public ExcelComparerPopupAll( ArrayList<ArrayList<ArrayList<String>>> data){
		setTitle("Excel Compared");
		setPreferredSize(new Dimension(maxWidth,maxHeight));
		java.util.ArrayList<JPanel> allPanels = new ArrayList<JPanel>();
		java.util.ArrayList<JTabbedPane> innerPanels = new ArrayList<JTabbedPane>();
		
		System.out.println("Size: "+data.size());
		for( int i = 0 ; i < data.size(); i++){
			JTabbedPane tabbedPane = new JTabbedPane();
			for( int j = 0 ; j < data.get(i).size() ; j++ ){
				if( i == j )continue;
				JPanel innerPannel = new JPanel();
				ArrayList<String> d1 = data.get(i).get(j);
				
				//System.out.println("array: "+d1.toString());
				innerPannel = new EXCELPopupComparer( this ,maxWidth,maxHeight-100, d1 );
				setLayout( new GridLayout(1,1));
				tabbedPane.add("Archive "+j,innerPannel);
			}
				
			
			innerPanels.add(tabbedPane);
		}
		if( innerPanels.size() > 1 ){
			setTitle("Compared");
			JTabbedPane tabbedPane = new JTabbedPane();
			for( int i = 0 ; i < innerPanels.size(); i++){
				tabbedPane.addTab("Archive "+i, innerPanels.get(i));
			}
			add(tabbedPane);
		}else
			add(innerPanels.get(0));
		
		addWindowListener(new ListenCloseWdw());
		mnuItemQuit.addActionListener(new ListenMenuQuit());


	}
	public class ListenMenuQuit implements ActionListener{
		  public void actionPerformed(ActionEvent e){
		             
		  }
	}
	public class ListenCloseWdw extends WindowAdapter{
		  public void windowClosing(WindowEvent e){
		                  
		  }
	}
}
