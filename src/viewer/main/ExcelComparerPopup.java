package viewer.main;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import viewer.BaseFrame;
import viewer.panels.DNAComparer;
import viewer.panels.DNAaminoacidretrieval;
import viewer.panels.DNApopupComparer;
import viewer.panels.EXCELPopupComparer;

public class ExcelComparerPopup extends BaseFrame {
	
	private JPanel panel1; 
	
	private final int maxWidth = 1000;
	private final int maxHeight = 700;
	
	public ExcelComparerPopup( ArrayList<ArrayList<String>> data){
		setTitle("Excel Compared");
		setPreferredSize(new Dimension(maxWidth,maxHeight));
		java.util.ArrayList<JPanel> allPanels = new ArrayList<JPanel>();
		
		System.out.println("Size: "+data.size());
		for( int i = 0 ; i < data.size(); i++){
			System.out.println("Value:"+data.get(i));
			JPanel topPanel = new JPanel();
			//allPanels.add(new JPanel());
			topPanel.setLayout( new BorderLayout() );
			ArrayList<String> d1 = data.get(i);
			
			System.out.println("array: "+d1.toString());
			topPanel = new EXCELPopupComparer( this ,maxWidth,maxHeight-50, d1 );
	
			setLayout( new GridLayout(1,1));
			allPanels.add(topPanel);
		}
		if( allPanels.size() > 1 ){
			setTitle("Compared");
			JTabbedPane tabbedPane = new JTabbedPane();
			for( int i = 0 ; i < allPanels.size(); i++){
				tabbedPane.addTab("Archive "+i, allPanels.get(i));
			}
			add(tabbedPane);
		}else
			add(allPanels.get(0));
		
		addWindowListener(new ListenCloseWdw());
		mnuItemQuit.addActionListener(new ListenMenuQuit());


	}
	public class ListenMenuQuit implements ActionListener{
		  public void actionPerformed(ActionEvent e){
		             
		  }
	}
	public class ListenCloseWdw extends WindowAdapter{
		  public void windowClosing(WindowEvent e){
		                  
		  }
	}
}
