package viewer.main;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import core.Context;

import locale.Language;

import viewer.BaseFrame;
import viewer.panels.DNAComparer;
import viewer.panels.DNAaminoacidretrieval;
import viewer.panels.DNAoperations;
import viewer.panels.EXCELselector;

public class MainGui extends BaseFrame{
	private	JTabbedPane tabbedPane;
	
	private		JPanel		panel1, panel2,panel3,panel4;
	JMenuItem cutAction, copyAction, pasteAction,proxyAction;
	private ArrayList<MainPanel> allPanels;
	
	public MainGui(){
		
		setTitle(Language.lang().getTranslation("main.title"));
		
		JPanel topPanel = new JPanel();
		topPanel.setLayout( new BorderLayout() );
		allPanels = new ArrayList<MainPanel>();
		//getContentPane().add( topPanel );
		
		tabbedPane = new JTabbedPane();
		panel1 = new DNAaminoacidretrieval( this );
		panel2 = new DNAComparer( this );
		panel3 = new DNAoperations( this );
		panel4 = new EXCELselector( this );
		allPanels.add((MainPanel) panel1);
		allPanels.add((MainPanel) panel2);
		allPanels.add((MainPanel) panel3);
		allPanels.add((MainPanel) panel4);
		tabbedPane.addTab( "Sequence", panel1 );
		tabbedPane.addTab( "Comparator", panel2 );
		tabbedPane.addTab( "Operations", panel3 );
		tabbedPane.addTab( "Excel comparer", panel4 );
		
		//topPanel.add( tabbedPane, BorderLayout.CENTER );
		setLayout( new GridLayout(1,1));
		add(tabbedPane);
		
		//addWindowListener(new ListenCloseWdw());
		//mnuItemQuit.addActionListener(new ListenMenuQuit());
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JMenu fileMenu = new JMenu("File");
        JMenu editMenu = new JMenu("Edit");
        JMenu optionsMenu = new JMenu("Options");
        JMenu helpMenu = new JMenu("Help");
        JMenuItem exitAction = new JMenuItem("Exit");
        exitAction.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                System.exit(0);
            }
        });
		cutAction = new JMenuItem("Cut");
		cutAction.addActionListener(new ListenCut(allPanels));
        copyAction = new JMenuItem("Copy");
        copyAction.addActionListener(new ListenCopy(allPanels));
        pasteAction = new JMenuItem("Paste");
        pasteAction.addActionListener(new ListenPaste(allPanels));
        fileMenu.add(exitAction);
        
        editMenu.add(cutAction);
        editMenu.add(copyAction);
        editMenu.add(pasteAction);
        
        proxyAction = new JMenuItem(Language.lang().getTranslation("main.menu.options.proxy"));
        proxyAction.addActionListener(new ProxyAction());
        optionsMenu.add(proxyAction);
        
        
        
        JMenuItem about = new JMenuItem("About");
        about.addActionListener( new ActionListener(){
        	public void actionPerformed(ActionEvent arg0 ){
        		/*BaseFrame frm = new BaseFrame();
        		AboutDialog abtDiag = new AboutDialog(frm);
        		frm.launchFrame();
        		frm.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);*/
        		AboutDialog abtDiag = new AboutDialog( new BaseFrame());
        		abtDiag.setResizable(false);
        		abtDiag.setVisible(true);
        		
        	}
        });
        helpMenu.add(about);

        // Creates a menubar for a JFrame
        JMenuBar menuBar = new JMenuBar();
        
        // Add the menubar to the frame
        setJMenuBar(menuBar);
        menuBar.add(fileMenu);
        menuBar.add(editMenu);
        menuBar.add(optionsMenu);
        menuBar.add( helpMenu );
        
        

	}
	public class ListenMenuQuit implements ActionListener{
		  public void actionPerformed(ActionEvent e){
		     System.exit(0);         
		  }
	}
	public class ListenCloseWdw extends WindowAdapter{
		  public void windowClosing(WindowEvent e){
		         System.exit(0);         
		  }
	}
	
	public class ListenCut implements ActionListener{
		private ArrayList<MainPanel> allPanels;
		public ListenCut( ArrayList<MainPanel> arr ){
			allPanels = arr;
		}
		public void actionPerformed(ActionEvent e){
			for( int i = 0 ; i < allPanels.size() ; i++){
				if( allPanels.get(i).isVisible()){
					allPanels.get(i).execute_cut();
				}
			}
	    }
	}
	public class ListenCopy implements ActionListener{
		private ArrayList<MainPanel> allPanels;
		public ListenCopy( ArrayList<MainPanel> arr ){
			allPanels = arr;
		}
		public void actionPerformed(ActionEvent e){
			for( int i = 0 ; i < allPanels.size() ; i++){
				if( allPanels.get(i).isVisible()){
					allPanels.get(i).execute_copy();
				}
			}
	    }
	}
	public class ListenPaste implements ActionListener{
		private ArrayList<MainPanel> allPanels;
		public ListenPaste( ArrayList<MainPanel> arr ){
			allPanels = arr;
		}
		public void actionPerformed(ActionEvent e){
			for( int i = 0 ; i < allPanels.size() ; i++){
				if( allPanels.get(i).isVisible()){
					allPanels.get(i).execute_paste();
				}
			}
	    }
	}
	public class ProxyAction implements ActionListener{
		private boolean state = false;
		@Override
		public void actionPerformed(ActionEvent arg0) {
			if( state ){
				Context.instance().setProxy();
				proxyAction.setText(proxyAction.getText().substring(2));
				state = false;
			}else{
				
				Context.instance().setProxy("localhost", 8080);
				proxyAction.setText("x "+proxyAction.getText());
				state = true;
			}
			
		}
		
	}
	
	
}
