package viewer.main;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import viewer.BaseFrame;
import viewer.panels.DNAComparer;
import viewer.panels.DNAaminoacidretrieval;
import viewer.panels.DNApopupComparer;

public class ComparerPopup extends BaseFrame {
	
	private JPanel panel1; 
	
	private final int maxWidth = 1000;
	private final int maxHeigth = 700;
	private final int widthWithProblem = maxWidth + 200;
	
	public ComparerPopup( java.util.List<String> data , java.util.List<java.util.List<String>> problemsFound){
		setTitle("DNA Compared");
		int width = (problemsFound.size()>0?widthWithProblem:maxWidth);
		setPreferredSize(new Dimension(width,maxHeigth));
		java.util.List<JPanel> allPanels = new ArrayList<JPanel>();
		
		for( int i = 0 ; i < data.size()/2; i++){
			JPanel topPanel = new JPanel();
			//allPanels.add(new JPanel());
			topPanel.setLayout( new BorderLayout() );
			java.util.List<String> d1 = data.subList(i*2, i*2+2);
			
			System.out.println("array: "+d1.toString());
			System.out.println("array1: "+problemsFound.get(i).toString());
			topPanel = new DNApopupComparer( this , width, maxHeigth-50, d1, problemsFound.get(i) );
	
			setLayout( new GridLayout(1,1));
			allPanels.add(topPanel);
		}
		if( allPanels.size() > 1 ){
			setTitle("Possible DNA alignments");
			JTabbedPane tabbedPane = new JTabbedPane();
			for( int i = 0 ; i < allPanels.size(); i++){
				tabbedPane.addTab("Possibility "+i, allPanels.get(i));
			}
			add(tabbedPane);
		}else
			add(allPanels.get(0));
		
		addWindowListener(new ListenCloseWdw());
		mnuItemQuit.addActionListener(new ListenMenuQuit());


	}
	public class ListenMenuQuit implements ActionListener{
		  public void actionPerformed(ActionEvent e){
		             
		  }
	}
	public class ListenCloseWdw extends WindowAdapter{
		  public void windowClosing(WindowEvent e){
		                  
		  }
	}
}
