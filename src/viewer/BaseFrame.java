package viewer;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import viewer.panels.AnimatedPanel;

import locale.Language;

import viewer.panels.DNAComparer;
import viewer.panels.DNAaminoacidretrieval;

public class BaseFrame extends JFrame{
	protected JMenuItem mnuItemQuit = new JMenuItem("Quit"); // Quit sub item
	private	JTabbedPane tabbedPane;
	
	private		JPanel		panel1; 
	private JPanel panel2;
	
	
	public BaseFrame(){
		//ImageIcon icon = new ImageIcon("content/images/dna.jpg");
		ImageIcon icon = new ImageIcon(getClass().getResource("/images/dna.jpg"));
		
		
		//getContentPane().setLayout(null);
		this.setIconImage(icon.getImage());
		
		
		//setTitle("DNA Checker");
		setTitle(Language.lang().getTranslation("main.title"));
		//setResizable(false);
		
		//setSize(620, 450);
		setPreferredSize(new Dimension(1055,570));
		//setPreferredSize(new Dimension(300,200));
		
		setLayout( new GridLayout(1,1));
		this.setResizable(false);
		
		//addWindowListener(new ListenCloseWdw());
		//mnuItemQuit.addActionListener(new ListenMenuQuit());
	}
	
	public class ListenMenuQuit implements ActionListener{
		  public void actionPerformed(ActionEvent e){
		     System.exit(0);         
		  }
	}
	public class ListenCloseWdw extends WindowAdapter{
		  public void windowClosing(WindowEvent e){
		         System.exit(0);         
		  }
	}
	public void launchFrame(){
		  // Display Frame
		  setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		  pack(); //Adjusts panel to components for display
		  setVisible(true);
	}
}
