/**
 * 
 */
package application;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author bluetc-1205
 *
 */
public class OperationsTest {

	/**
	 * Test method for {@link application.Operations#checkDifference(java.lang.String, java.lang.String)}.
	 */
	@Test
	public void testCheckDifference() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link application.Operations#getAminoacidFASTa(java.lang.String)}.
	 */
	@Test
	public void testGetAminoacidFASTa() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link application.Operations#checkSequenceFASTa(java.lang.String, boolean)}.
	 */
	@Test
	public void testCheckSequenceFASTa() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link application.Operations#convertFromFASTa(java.lang.String)}.
	 */
	@Test
	public void testConvertFromFASTa() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link application.Operations#retrieveTwoDNAFromFASTa(java.lang.String, java.lang.String)}.
	 */
	@Test
	public void testRetrieveTwoDNAFromFASTa() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link application.Operations#retrieveComparedDNASeqFASTa(java.lang.String, java.lang.String)}.
	 */
	
	public void testRetrieveComparedDNASeqFASTaEqual() {
		String sequence1 = new String("ATGTGAATGTGA");
		String sequence2 = new String("ATGTGAATGTGA");
		//char[][] result = Operations.retrieveComparedDNASeqFASTa(sequence1, sequence2);
		//Assert.assertArrayEquals(result[0], result[1]);
	}
	
	public void testRetrieveComparedDNASeqFASTaDiff() {
		String sequence1 = new String("ATGTGAATGTGA");
		String sequence2 = new String("ATGTGACTGTGA");
		String expected1 = new String("ATGTGA******ATGTGA");
		String expected2 = new String("ATGTGACTGTGA??????");
		//char[][] result = Operations.retrieveComparedDNASeqFASTa(sequence1, sequence2);
		//System.out.println("Result[0]="+(new String(result[0])));
		//System.out.println("Result[1]="+(new String(result[1])));
		//Assert.assertArrayEquals(expected1.toCharArray(), result[0]);
		//Assert.assertArrayEquals(expected2.toCharArray(), result[1]);
	}

	
	public String test1( String seq1, String seq2 ) {
		System.out.println("seq1:" + seq1);
		System.out.println("seq2:" + seq2);
		String result;
		if( seq1.length() == 0 && seq2.length() == 0 ){
			System.out.println("both: empty");
			return new String();
		}else if( (seq1.length() != 0 && seq2.length() != 0) &&(seq1.charAt(0) == seq2.charAt(0))){
			System.out.println("both: equal");
			return seq1.charAt(0)+test1( seq1.substring(1),seq2.substring(1));
		}else{
			 if( seq1.length() == 0){
				 System.out.println("seq1: empty");
				 return seq2.charAt(0)+test1( seq1 ,seq2.substring(1));
			}else if( seq2.length() == 0){
				System.out.println("seq2: empty");
				return seq1.charAt(0)+test1( seq1.substring(1) ,seq2);
			}else{
				System.out.println("Just diferent");
				String res1 = test1(seq1,seq2.substring(1));
				String res2 = test1(seq1.substring(1),seq2);
				int grade1 = grade(res1);
				int grade2 = grade(res2);
				if( grade1 > grade2 )
					result = seq1.charAt(0)+res1;
				else
					result = seq2.charAt(0)+res2;
				return result;
			}
			
		}
	}
	
	public int grade( String seq ){
		//System.out.println("grade: seq:"+seq);
		if( seq.length() == 0 )
			return -1;
		if(!seq.contains("*"))
			return 0;
		return countOccurrences(seq,'*');
		/*String[] t = seq.split("*");
		System.out.println("The T is: "+t.length);
		return t.length;*/
	}
	public static int countOccurrences(String haystack, char needle)
	{
	    int count = 0;
	    for (int i=0; i < haystack.length(); i++)
	    {
	        if (haystack.charAt(i) == needle)
	        {
	             count++;
	        }
	    }
	    return count;
	}
	@Test
	public void test(){
		System.out.println("test()");
		String sequence1 = new String("ACGGCTC");
		String sequence2 = new String("ATGGCCTC");
		String expected1 = new String("ATGTGA******ATGTGA");
		String expected2 = new String("ATGTGACTGTGA??????");
		//sequence1 = new String("ATG");
		//sequence2 = new String("ATC");
		
		smith(sequence1,sequence2);
		//String result = a(sequence1, sequence2);
		//System.out.println("Result="+result);
	}
	
	public String a( String seq1 , String seq2 ){
		//System.out.println("seq1:[" + seq1+"], seq2:[" + seq2+"]");
		if( seq1.length() != 0 && seq1.equals(seq2 ) ){
			//System.out.println("Equal");
			return seq1;
		}
		if( seq1.length() == 0 ||  seq2.length() == 0)
			return new String();
		
		//System.out.println("Start first branch");
		String res1 =a(seq1.substring(1),seq2);
		//System.out.println("Ended first branch");
		String res2 =a(seq1,seq2.substring(1));
		//System.out.println("Ended second branch");
		//System.out.println("res1:["+res1+"] res2:["+res2+"]");
		int grade1 = grade(res1);
		int grade2 = grade(res2);
		
		if( grade1 < grade2 )
			return "*"+res1;
		else
			return "*"+res2;
		
	}
	

	public void smith(String seq1, String seq2){
		System.out.println("seq1:[" + seq1+"], seq2:[" + seq2+"]");
		int[][] arr = new int[seq2.length()][seq1.length()];
		int max = 0;
		int x = 0;
		int y = 0;
		for( int i = 0 ; i < seq1.length(); i++ ){
			arr[0][i] = (seq1.charAt(i)==seq2.charAt(0)?1:0);
		}
		for( int i = 1 ; i < seq2.length(); i++ ){
			arr[i][0] = (seq1.charAt(0)==seq2.charAt(i)?1:0);
		}
		System.out.println(Arrays.deepToString(arr));
		
		for( int i = 1; i < seq2.length() ; i++ ){
			for( int j = 1 ; j < seq1.length() ; j++ ){
				if(seq1.charAt(j)==seq2.charAt(i)){
					arr[i][j] = 1 + arr[i-1][j-1];
				}
			}
		}
		System.out.println(Arrays.deepToString(arr));
		
		
	}
	
	@Test
	public void testRetrieveComparedDNASeqFASTa(){
		String seq1 = new String(	"ATGTCAAACATTGATTTTAAAGCATTAGAAAGAGCAGCAAATGAAACTAGAGGTTTAAGC"+
									"ATGGATGCAGTTGCCAAAGCTGCCTCTGGTCATTTAGGTTTACCACTTGGTTCTGCTGAA"+
									"ATTGGTGCTGCATTATTTGGTAATTCATTGATCTATAACCCAAAAGATACAAGATGGTTA"+
									"AATAGAGATTATTTTGTATTAAGTGCAGGTCATGGTTCAATGTTTTTATATAGTTGGTTA"+
									"CATTTATCAGGATATGATGTTTCAATTGAAGATATTAAAAATTTCAGACAATTAAATTCA"+
									"AAGACACCAGGTCATCCAAAATTTCATGACACACCAGGCGTTGAAGCTACCACTGGTCCA"+
									"TTAGGTCAAGGTATTGCAAATGCCGTTGGTATTGCATCAGCATGTAAAATGGCAGCAGGT"+
									"AAATTCAATACTGAACAACATCAAATCTTTAATCAAAAGGTAGTAGTTTTAGTTGGTGAT"+
									"GGTTGTTTACAAGAAGGTATTTCACAAGAGGCCATTTCATTCGCTGGTCATCATCGTTTA"+
									"GATAACTTGATCGTATTCTACGATTCAAACGATGTCACATTGGATGCTATGGCAATTGAA"+
									"ACCCAATCAGAGGATGCCGTAAAGAGATTCGAAAGTGTTGGATTCGAAGTACAATTAGTA"+
									"TTAGAAGGTAACAACATTGGCTCATTAATCAACGCCTATCAAAATGCTAAACACTCCAAA"+
									"TCCGGTAAACCACAAATCATCATTTGTAAGACTACAATCGCCAAGGGTATCCCAGAGGTT"+
									"GCTGGTACCAATAAAGGTCATGGTGAAGCTGGTGTTAAATTTATCGATTCCGCTCGTAAG"+
									"AATCTCGGTCTCCCAGAAGAGAAATTCTTTGTCAGCGGTGAAACCAGACAATACTTTGAA"+
									"CAACATGAAAAACAATTAGAAAAGCTCTACCAAGAATGGCAAGCAACCTTTGCCGAATGG"+
									"AAATCTGCCAATCCAAAGCTTGCCCAACTCTTGGAAAGTGCTCATGAAAAACACGAAGCT"+
									"ATCGATATCATGAAACAAATCCCAGAGTTCCCAACCACACCAATCATTGCCACTCGTAAG"+
									"GCTGGTAGCGACGTTTTACAACCAATCTCTCAATACCTCCCATTATCAGTCAGTGGTAGT"+
									"GCCGATTTACATGGCTCCACCTTGAATTACATCAAAGAGGGTAGAGATTTCACACCAGCT"+
									"TGTCCAACTGGTCGTAATATTAAGTTTGGTATTCGTGAACATGCTATGGGTGCCATGATG"+
									"AATGGTATCGCCTACCATGGTCTTTTCAAAGTTTCAGGTGCCACCTTTTTGGTATTCTCA"+
									"GACTATCTTCGTCCAGCCATCCGTTTGGCTGCTCTCTCACATTTACCAGTAGTTTACATT"+
									"TTCACTCATGACTCTGTTGGTGTTGGTGAAGATGGTCCAACTCATCAACCAGTTGAAACC"+
									"GTTTCAGGTCTTCGTATGATTCCAAACTTGGATGTCATTCGTCCAGCCGATCCAGAAGAA"+
									"ACAGCTGCTGCTTTCTCTTTGGCTTATGCTCGTGCCGATGGTCCAACCCTTTTATCATTA"+
									"ACTCGTCAAAATCTTCCATTCCTCCCAGGTACCGCTCAAAAGAAGAGAGAAGGTACTCTC"+
									"AGAGGTGGTTACATTGTCGTTAGTGAAACCGCTCCATTAAGAATGATCTTAATTGCCACT"+
									"GGTTCAGAAGTTCAACATTGTGTTGAAGCCGCTAAATTATTAGGTGATGATATTCGTGTC"+
									"GTCTCTATGCCATGCACTGAACTTTTCGATCGTCAATCAAATGAATACAAACAATCCGTA"+
									"CTTCCAAGTGGTTGTAGAAATCGTATCGCTATGGAAGCTGGTGTCACTTCATTCTGGTAT"+
									"AAATATGTTGGTCTTGATGGTAAAGTCATTGGTATTGATAGATTTGGTATGTCTGCACCA"+
									"GGTAATGCCGTTATGAAACAATTAGGTATGACTTCTGAAAATTTAGTAAACATTTCAAAA"+
									"CAATAA");
		
		//String[] res = Operations.retrieveComparedDNASeqFASTa(seq1, seq1);
	}

}
