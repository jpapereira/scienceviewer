package core;

import static org.junit.Assert.*;
import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

public class MessageQueueTest {

	@Before
	public void testClearQueue(){
		MessageQueue.getQueue().clearQueue();
	}

	@Test
	public void testAddMessage() {
		Assert.assertEquals("Error adding Message!!",true, MessageQueue.getQueue().addMessage("Test"));
	}

	@Test
	public void testGetMessage() {
		Assert.assertEquals("Error adding Message!!",true, MessageQueue.getQueue().addMessage("Test"));
		String result = MessageQueue.getQueue().getMessage();
		System.out.println("The message in the queue is: "+result);
		Assert.assertEquals("The message in the queue is not the inserted!!",new String("Test"), result);
	}

	@Test
	public void testGetNumberMessages() {
		Assert.assertEquals("Error adding Message!!",true, MessageQueue.getQueue().addMessage("Test"));
		Assert.assertEquals("The number of messages in the queue is different then 1!",1, MessageQueue.getQueue().getNumberMessages());
		
	}
	
	@Test
	public void testLimitOfQueueMax(){
		for( int i = 0 ; i < 100 ; i++ ){
			Assert.assertEquals("Error adding Message!!",true, MessageQueue.getQueue().addMessage("Test"));
		}
		Assert.assertEquals("Passed the limit!!",false, MessageQueue.getQueue().addMessage("Test"));
	}
	
	@Test
	public void testLimitOfQueueMin(){
		for( int i = 0 ; i < 20 ; i++ ){
			System.out.println("Adding message int position: "+i);
			MessageQueue.getQueue().addMessage("Test");
			Assert.assertEquals("Number of messages is not correct!!",(i+1),MessageQueue.getQueue().getNumberMessages());
		}
		MessageQueue.getQueue().printQueue();
		for( int i = 0 ; i < 20 ; i++ ){
			System.out.println("Position "+i);
			Assert.assertEquals("The message in the queue is not the same!!",new String("Test"), MessageQueue.getQueue().getMessage());
		}
		Assert.assertEquals("There is one more message then expected!!",null, MessageQueue.getQueue().getMessage());
	}

}
